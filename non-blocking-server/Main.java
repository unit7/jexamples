import java.nio.file.*;
import java.io.*;
import java.util.Random;

public class Main {

	public static void main(String[] args) {

		Random random = new Random();
		long type = 0;
		int value = 500;
		long mask;

		try (DataOutputStream out = new DataOutputStream(Files.newOutputStream(Paths.get("example_data.ff")))) {
			for (int i = 0; i < 1000; ++i) {
				type = random.nextInt(4);
				mask = (type << 32) | value;
				int latitude = random.nextInt();
				int longitude = random.nextInt();

				out.writeLong(mask);
				out.writeInt(latitude);
				out.writeInt(longitude);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}