package com.unit7.example.nonblocking;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openjdk.jmh.annotations.*;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Created by breezzo on 01.05.16.
 */
@State(Scope.Benchmark)
@Threads(60)
@Fork(1)
@Measurement(iterations = 20)
@Warmup(iterations = 5)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
public class PerformanceTest {

    private final int loopCount = 1;

    private static Client client;
    private static WebResource resource;
    private static Gson gson;

    @Setup
    @BeforeClass
    public static void setup() {
        client = Client.create();
        resource = client.resource("http://localhost:8080");
        gson = new Gson();
    }

    @TearDown
    @AfterClass
    public static void tearDown() {
        client.destroy();
    }

    @Test
    @Benchmark
    public void testMultithreaded() throws IOException {

        for (int i = 0; i < loopCount; ++i) {
            ClientResponse response = resource.path("/fruiter/location/fruit/APPLE").get(ClientResponse.class);
            Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        }
    }

    @Test
    @Benchmark
    public void testSync() throws IOException {

        for (int i = 0; i < loopCount; ++i) {
            ClientResponse response = resource.path("/fruiter/location/fruit/sync/APPLE").get(ClientResponse.class);
            Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        }
    }
}
