package com.unit7.example.nonblocking.service.fruit.impl;

import com.unit7.example.nonblocking.model.Location;
import com.unit7.example.nonblocking.model.storage.FruitStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Supplier;

/**
 * Created by breezzo on 02.05.16.
 */
class FruitReader implements Supplier<FruitStorage> {
    private static final Logger logger = LoggerFactory.getLogger(FruitReader.class);

    @Override
    public FruitStorage get() {
        // stupid method :)
        logger.info("start reading storage base");

        Path storage = StorageLocation.getStorageLocation();

        try (InputStream stream = new BufferedInputStream(Files.newInputStream(storage))) {
            return readContent(stream);
        } catch (IOException e) {
            logger.error("Some error occurred while read storage", e.getMessage());
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private FruitStorage readContent(InputStream stream) throws IOException {
        Collection<FruitStorage.Entry> entries = new ArrayList<>();
        FruitStorage storage = new FruitStorage();
        storage.setStorageEntries(entries);

        try (DataInputStream dataStream = new DataInputStream(stream)) {
            while (dataStream.available() != 0) {
                long mask = dataStream.readLong();
                int latitude = dataStream.readInt();
                int longitude = dataStream.readInt();

                FruitStorage.Entry entry = new FruitStorage.Entry();
                entry.setFruitTypeMask(mask);
                entry.setLocation(new Location(latitude, longitude));

                entries.add(entry);

                logger.trace("entry [ {} ] readed", entry);
            }
        }

        return storage;
    }
}
