package com.unit7.example.nonblocking.service.fruit.impl;

import com.unit7.example.nonblocking.model.Fruit;
import com.unit7.example.nonblocking.model.FruitLocation;
import com.unit7.example.nonblocking.model.Location;
import com.unit7.example.nonblocking.model.storage.FruitStorage;
import com.unit7.example.nonblocking.web.rest.FruitType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by breezzo on 02.05.16.
 */
class FruitAggregator implements Function<FruitStorage, Collection<Location>> {

    private static final Logger logger = LoggerFactory.getLogger(FruitAggregator.class);

    private FruitType fruitType;

    public FruitAggregator(FruitType fruitType) {
        this.fruitType = fruitType;
    }

    @Override
    public Collection<Location> apply(FruitStorage storage) {
        logger.info("transform and find locations");
        return storage.getStorageEntries()
                .stream()
                .map(entry -> FruitLocation.of(Fruit.of(entry), entry.getLocation()))
                .filter(fruitLocation -> fruitLocation.getFruit().getFruitType() == fruitType)
                .map(FruitLocation::getLocation)
                .collect(Collectors.toList());
    }
}
