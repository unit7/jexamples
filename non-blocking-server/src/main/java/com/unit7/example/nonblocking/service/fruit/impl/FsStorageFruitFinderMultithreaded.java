package com.unit7.example.nonblocking.service.fruit.impl;

import com.unit7.example.nonblocking.model.Location;
import com.unit7.example.nonblocking.service.fruit.FruitFinder;
import com.unit7.example.nonblocking.service.threads.ExecutorService;
import com.unit7.example.nonblocking.service.threads.ExecutorServiceFactory;
import com.unit7.example.nonblocking.web.rest.FruitType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.concurrent.CompletableFuture;

/**
 * Created by breezzo on 01.05.16.
 */
public class FsStorageFruitFinderMultithreaded implements FruitFinder {

    private static final Logger logger = LoggerFactory.getLogger(FsStorageFruitFinderMultithreaded.class);

    private ExecutorService executorService = ExecutorServiceFactory.getInstance().executorService();

    @Override
    public CompletableFuture<Collection<Location>> find(FruitType fruitType) {
        logger.info("try to find fruit {}", fruitType);
        return CompletableFuture
                .supplyAsync(new FruitReader(), executorService.readExecutor())
                .thenApplyAsync(new FruitAggregator(fruitType), executorService.findExecutor());
    }
}
