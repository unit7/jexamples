package com.unit7.example.nonblocking.web.rest;

import com.unit7.example.nonblocking.model.Location;
import com.unit7.example.nonblocking.service.fruit.FruitFinder;
import com.unit7.example.nonblocking.service.fruit.FruitFinderFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collection;
import java.util.concurrent.ExecutionException;

/**
 * Created by breezzo on 01.05.16.
 */
@Path("/location/fruit")
public class LocationFinder {

    private static final Logger logger = LoggerFactory.getLogger(LocationFinder.class);

    @GET
    @Path("{fruitType}")
    @Produces(MediaType.APPLICATION_JSON)
    public void findFruit(@PathParam("fruitType") FruitType fruitType, @Suspended AsyncResponse response) {
        logger.debug("findFruit {}", fruitType);

        FruitFinder finder = FruitFinderFactory.getInstance().fruitFinderMultithreaded();
        finder.find(fruitType).thenAcceptAsync(response::resume).exceptionally(throwable -> {
            logger.error("Error fruit finding", throwable);
            response.resume(throwable);
            return null;
        });
    }

    @GET
    @Path("/sync/{fruitType}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findFruit(@PathParam("fruitType") FruitType fruitType) throws ExecutionException, InterruptedException {
        logger.debug("findFruit {} sync", fruitType);

        FruitFinder finder = FruitFinderFactory.getInstance().simpleFruitFinder();
        Collection<Location> result = finder.find(fruitType).get();

        return Response.ok(result).build();
    }
}
