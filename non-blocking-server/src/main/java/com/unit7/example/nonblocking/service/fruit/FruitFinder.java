package com.unit7.example.nonblocking.service.fruit;

import com.unit7.example.nonblocking.model.Location;
import com.unit7.example.nonblocking.web.rest.FruitType;

import java.util.Collection;
import java.util.concurrent.CompletableFuture;

/**
 * Created by breezzo on 01.05.16.
 */
public interface FruitFinder {

    CompletableFuture<Collection<Location>> find(FruitType fruitType);
}
