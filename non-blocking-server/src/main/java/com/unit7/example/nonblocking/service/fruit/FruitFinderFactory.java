package com.unit7.example.nonblocking.service.fruit;

import com.unit7.example.nonblocking.service.fruit.impl.FsStorageFruitFinderMultithreaded;
import com.unit7.example.nonblocking.service.fruit.impl.FsStorageFruitFinderSimple;

/**
 * Created by breezzo on 01.05.16.
 */
public class FruitFinderFactory {

    public static FruitFinderFactory getInstance() {
        return new FruitFinderFactory();
    }

    public FruitFinder fruitFinderMultithreaded() {
        return new FsStorageFruitFinderMultithreaded();
    }

    public FruitFinder simpleFruitFinder() {
        return new FsStorageFruitFinderSimple();
    }
}
