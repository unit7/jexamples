package com.unit7.example.nonblocking.service.threads;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.*;

/**
 * Created by breezzo on 01.05.16.
 */
public class ExecutorServiceImpl implements ExecutorService {

    private static final Logger logger = LoggerFactory.getLogger(ExecutorServiceImpl.class);

    private volatile Executor readExecutor;
    private volatile Executor findExecutor;

    private final Object readLock = new Object();
    private final Object findLock = new Object();

    @Override
    public Executor readExecutor() {
        logger.debug("try to get read executor");

        Executor executor = readExecutor;
        if (executor == null) {
            synchronized (readLock) {
                executor = readExecutor;
                if (executor == null) {
                    executor = new ThreadPoolExecutor(5,30, 60, TimeUnit.SECONDS, new LinkedBlockingQueue<>(), new ThreadFactory("read"));
                    readExecutor = executor;
                    logger.debug("readExecutor initialized");
                }
            }
        }

        return executor;
    }

    @Override
    public Executor findExecutor() {
        logger.debug("try to get find executor");

        Executor executor = findExecutor;
        if (executor == null) {
            synchronized (findLock) {
                executor = findExecutor;
                if (executor == null) {
                    executor = Executors.newCachedThreadPool(new ThreadFactory("find"));
                    findExecutor = executor;
                    logger.debug("findExecutor initialized");
                }
            }
        }

        return executor;
    }
}
