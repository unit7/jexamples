package com.unit7.example.nonblocking.service.threads;

/**
 * Created by breezzo on 01.05.16.
 */
public class ExecutorServiceFactory {

    public static ExecutorServiceFactory getInstance() {
        return new ExecutorServiceFactory();
    }

    public ExecutorService executorService() {
        return new ExecutorServiceImpl();
    }
}
