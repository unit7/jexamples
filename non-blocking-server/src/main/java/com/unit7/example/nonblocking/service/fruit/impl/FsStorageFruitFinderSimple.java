package com.unit7.example.nonblocking.service.fruit.impl;

import com.unit7.example.nonblocking.model.Location;
import com.unit7.example.nonblocking.model.storage.FruitStorage;
import com.unit7.example.nonblocking.service.fruit.FruitFinder;
import com.unit7.example.nonblocking.web.rest.FruitType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.concurrent.CompletableFuture;

/**
 * Created by breezzo on 02.05.16.
 */
public class FsStorageFruitFinderSimple implements FruitFinder {
    private static final Logger logger = LoggerFactory.getLogger(FsStorageFruitFinderSimple.class);

    @Override
    public CompletableFuture<Collection<Location>> find(FruitType fruitType) {
        logger.info("try find fruit {} in one thread", fruitType);

        FruitReader reader = new FruitReader();
        FruitAggregator aggregator = new FruitAggregator(fruitType);

        FruitStorage storage = reader.get();
        Collection<Location> result = aggregator.apply(storage);

        return CompletableFuture.completedFuture(result);
    }
}
