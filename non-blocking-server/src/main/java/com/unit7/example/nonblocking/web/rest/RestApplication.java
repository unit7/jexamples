package com.unit7.example.nonblocking.web.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by breezzo on 01.05.16.
 */
@ApplicationPath("fruiter")
public class RestApplication extends Application {
    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> classes = new HashSet<>();
        classes.add(LocationFinder.class);
        return classes;
    }
}
