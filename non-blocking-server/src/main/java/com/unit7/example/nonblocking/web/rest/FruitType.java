package com.unit7.example.nonblocking.web.rest;

/**
 * Created by breezzo on 01.05.16.
 */
public enum FruitType {
    APPLE,
    ORANGE,
    BANANA,
    CHERRY
}
