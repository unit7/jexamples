package com.unit7.example.nonblocking.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by breezzo on 01.05.16.
 */
public class FruitLocation {

    private static final Logger logger = LoggerFactory.getLogger(FruitLocation.class);

    private Fruit fruit;
    private Location location;

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Fruit getFruit() {
        return fruit;
    }

    public void setFruit(Fruit fruit) {
        this.fruit = fruit;
    }

    public static FruitLocation of(Fruit fruit, Location location) {
        FruitLocation fruitLocation = new FruitLocation();
        fruitLocation.setFruit(fruit);
        fruitLocation.setLocation(location);

        logger.trace("fruit location constructed {}", fruitLocation);

        return fruitLocation;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("FruitLocation{");
        sb.append("fruit=").append(fruit);
        sb.append(", location=").append(location);
        sb.append('}');
        return sb.toString();
    }
}
