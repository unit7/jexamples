package com.unit7.example.nonblocking.service.threads;

import java.util.concurrent.Executor;

/**
 * Created by breezzo on 01.05.16.
 */
public interface ExecutorService {

    Executor readExecutor();

    Executor findExecutor();
}
