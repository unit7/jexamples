package com.unit7.example.nonblocking.service.threads;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by breezzo on 03.05.16.
 */
public class ThreadFactory implements java.util.concurrent.ThreadFactory {
    private final String suffix;
    private final ThreadGroup group;
    private final AtomicInteger threadCount = new AtomicInteger(0);

    public ThreadFactory(String suffix) {
        this.suffix = suffix;
        SecurityManager s = System.getSecurityManager();
        group = (s != null) ? s.getThreadGroup() :
                Thread.currentThread().getThreadGroup();
    }


    @Override
    public Thread newThread(Runnable r) {
        return new Thread(group, r, "fs-" + suffix + "-" + threadCount.incrementAndGet());
    }
}
