package com.unit7.example.nonblocking.model;

import com.unit7.example.nonblocking.model.storage.FruitStorage;
import com.unit7.example.nonblocking.web.rest.FruitType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by breezzo on 01.05.16.
 */
public class Fruit {
    private static final Logger logger = LoggerFactory.getLogger(Fruit.class);

    private int value;
    private FruitType fruitType;

    public FruitType getFruitType() {
        return fruitType;
    }

    public void setFruitType(FruitType fruitType) {
        this.fruitType = fruitType;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public static Fruit of(FruitStorage.Entry entry) {
        long mask = entry.getFruitTypeMask();
        int value = (int) (mask & 0xFFFFFFFFL);
        int fruitIndex = (int) ((mask >> 32) & 0xFFFFFFFFL);

        logger.trace("mask {}, value {}, fruitIndex {}", mask, value, fruitIndex);

        FruitType fruitType = FruitType.values()[fruitIndex];
        Fruit fruit = new Fruit();
        fruit.setFruitType(fruitType);
        fruit.setValue(value);

        return fruit;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Fruit{");
        sb.append("value=").append(value);
        sb.append(", fruitType=").append(fruitType);
        sb.append('}');
        return sb.toString();
    }
}
