package com.unit7.example.nonblocking.model.storage;

import com.unit7.example.nonblocking.model.Location;

import java.util.Collection;

/**
 * Created by breezzo on 01.05.16.
 */
public class FruitStorage {

    private Collection<Entry> storageEntries;

    public Collection<Entry> getStorageEntries() {
        return storageEntries;
    }

    public void setStorageEntries(Collection<Entry> storageEntries) {
        this.storageEntries = storageEntries;
    }

    public static class Entry {
        private Location location;
        private long fruitTypeMask;

        public long getFruitTypeMask() {
            return fruitTypeMask;
        }

        public void setFruitTypeMask(long fruitTypeMask) {
            this.fruitTypeMask = fruitTypeMask;
        }

        public Location getLocation() {
            return location;
        }

        public void setLocation(Location location) {
            this.location = location;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("Entry{");
            sb.append("location=").append(location);
            sb.append(", fruitTypeMask=").append(fruitTypeMask);
            sb.append('}');
            return sb.toString();
        }
    }
}
