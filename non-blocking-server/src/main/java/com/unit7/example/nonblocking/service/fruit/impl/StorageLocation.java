package com.unit7.example.nonblocking.service.fruit.impl;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by breezzo on 01.05.16.
 */
public class StorageLocation {

    public static Path getStorageLocation() {
        return Paths.get("/home/breezzo/sources/Idea/Java/examples/non-blocking-server/test/resources/example_data.ff");
    }
}
