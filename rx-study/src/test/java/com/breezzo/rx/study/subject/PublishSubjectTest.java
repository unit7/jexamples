package com.breezzo.rx.study.subject;

import org.junit.Assert;
import org.junit.Test;
import rx.subjects.PublishSubject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by breezzo on 14.07.16.
 */
public class PublishSubjectTest {
    @Test
    public void test() {
        List<Integer> result = new ArrayList<>();
        PublishSubject<Integer> subject = PublishSubject.create();
        subject.onNext(1);
        subject.subscribe(result::add);
        subject.onNext(2);
        subject.onNext(3);
        subject.onNext(4);

        Assert.assertTrue(result.containsAll(Arrays.asList(2, 3, 4)));
    }
}
