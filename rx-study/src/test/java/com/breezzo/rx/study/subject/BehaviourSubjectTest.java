package com.breezzo.rx.study.subject;

import org.junit.Assert;
import org.junit.Test;
import rx.subjects.BehaviorSubject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by breezzo on 15.07.16.
 */
public class BehaviourSubjectTest {
    @Test
    public void test() {
        List<Integer> result = new ArrayList<>();
        BehaviorSubject<Integer> subject = BehaviorSubject.create(1);
        subject.subscribe(result::add);
        subject.onNext(2);

        Assert.assertTrue(Arrays.asList(1, 2).containsAll(result));

        List<Integer> lateResult = new ArrayList<>();
        subject.subscribe(lateResult::add);

        Assert.assertTrue(Collections.singletonList(2).containsAll(lateResult));
    }
}
