package com.breezzo.rx.study.subject;

import org.junit.Assert;
import org.junit.Test;
import rx.subjects.ReplaySubject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by breezzo on 14.07.16.
 */
public class ReplaySubjectTest {
    @Test
    public void test() {
        List<Integer> early = new ArrayList<>();
        List<Integer> late = new ArrayList<>();

        ReplaySubject<Integer> subject = ReplaySubject.create();
        subject.subscribe(early::add);
        subject.onNext(1);
        subject.onNext(2);
        subject.subscribe(late::add);
        subject.onNext(3);

        Assert.assertTrue(early.containsAll(Arrays.asList(1, 2, 3)));
        Assert.assertTrue(late.containsAll(Arrays.asList(1, 2, 3)));

        late.clear();

        ReplaySubject<Integer> sizeSubject = ReplaySubject.createWithSize(2);
        sizeSubject.subscribe();
        sizeSubject.onNext(1);
        sizeSubject.onNext(2);
        sizeSubject.onNext(3);
        sizeSubject.subscribe(late::add);
        sizeSubject.onNext(4);

        Assert.assertTrue(late.containsAll(Arrays.asList(2, 3, 4)));
    }
}
