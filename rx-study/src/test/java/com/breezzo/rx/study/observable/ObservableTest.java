package com.breezzo.rx.study.observable;

import org.junit.Assert;
import org.junit.Test;
import rx.Observable;
import rx.functions.Actions;
import rx.functions.Func0;
import rx.subjects.PublishSubject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by breezzo on 15.07.16.
 */
public class ObservableTest {
    @Test
    public void rangeTest() {
        List<Integer> result = new ArrayList<>();
        Observable<Integer> ob = Observable.range(1, 5);
        ob.subscribe(result::add);

        Assert.assertTrue(Arrays.asList(1, 2, 3, 4, 5).containsAll(result));
    }

    @Test
    public void concatTest() {
        List<Integer> firstResult = new ArrayList<>();
        List<Integer> secondResult = new ArrayList<>();

        PublishSubject<Integer> first = PublishSubject.create();
        first.onNext(10);

        Observable<Integer> ob = Observable.concat(
                first,
                Observable.just(4, 5),
                Observable.range(1, 2),
                Observable.defer(() -> Observable.create(s -> {
                    s.onNext(1);
                    s.onCompleted();
                }))
        );

        ob.subscribe(firstResult::add);

        first.onNext(15);

        ob.subscribe(secondResult::add);

        first.onNext(22);
        first.onCompleted();
        first.onNext(11);

        Assert.assertTrue(Arrays.asList(15, 22, 4, 5, 1, 2, 1).containsAll(firstResult));
        Assert.assertTrue(Arrays.asList(22, 4, 5, 1, 2, 1).containsAll(secondResult));
    }
}
