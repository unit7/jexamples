package com.breezzo.rx.study.subject;

import org.junit.Assert;
import org.junit.Test;
import rx.Subscription;
import rx.functions.Action1;
import rx.functions.Actions;
import rx.subjects.PublishSubject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by breezzo on 15.07.16.
 */
public class SubjectSubscribeTest {

    @Test
    public void onComplete() {
        List<Integer> result = new ArrayList<>();

        PublishSubject<Integer> subject = PublishSubject.create();
        subject.subscribe(result::add);
        subject.onNext(1);
        subject.onNext(2);
        subject.onNext(3);
        subject.onCompleted();
        subject.onNext(4);

        Assert.assertTrue(Arrays.asList(1,2 ,3).containsAll(result));
    }

    @Test
    public void onError() {
        List<Integer> result = new ArrayList<>();

        PublishSubject<Integer> subject = PublishSubject.create();
        subject.subscribe(result::add, Actions.empty());
        subject.onNext(1);
        subject.onNext(2);
        subject.onNext(3);
        subject.onError(new Exception());
        subject.onNext(4);

        Assert.assertTrue(Arrays.asList(1,2 ,3).containsAll(result));
    }

    @Test
    public void unsubscribe() {
        List<Integer> result = new ArrayList<>();

        PublishSubject<Integer> subject = PublishSubject.create();
        Subscription subscription = subject.subscribe(result::add);
        subject.onNext(1);
        subject.onNext(2);
        subject.onNext(3);

        subscription.unsubscribe();
        Assert.assertTrue(subscription.isUnsubscribed());

        subject.onNext(4);

        Assert.assertTrue(Arrays.asList(1,2 ,3).containsAll(result));
    }
}