package com.breezzo.rx.study.subject;

import org.junit.Assert;
import org.junit.Test;
import rx.subjects.AsyncSubject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by breezzo on 15.07.16.
 */
public class AsyncSubjectTest {
    @Test
    public void test() {
        List<Integer> result = new ArrayList<>();
        AsyncSubject<Integer> subject = AsyncSubject.create();
        subject.subscribe(result::add);
        subject.onNext(1);
        subject.onNext(2);
        subject.onNext(3);
        subject.onCompleted();

        Assert.assertTrue(Collections.singletonList(3).containsAll(result));
    }
}
