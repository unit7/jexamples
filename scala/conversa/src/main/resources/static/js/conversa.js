/**
 * Created by breezzo on 05.09.15.
 */

localStorage.setItem('rootPath', '/conversa');
localStorage.setItem('staticPath', '/static');

angular.module('conversa', ['ngSanitize'])
    .directive('conversaMain', function() {
        var rootPath = localStorage.getItem('rootPath');
        var staticPath = localStorage.getItem('staticPath');

        return {
            restrict: 'A',
            templateUrl: rootPath + staticPath + '/html/conversa_main_template.html',
            link: function(scope, element, attrs) {
                scope.authenticated = false;
                scope.messageStorage = [];
            },
            controller: ['$scope', '$sce', function($scope, $sce) {

                var wsPath = 'ws://' + window.location.host + '/ws';
                var webSocket = new WebSocket(wsPath);

                webSocket.onmessage = function(e) {
                    var data = angular.fromJson(e.data);
                    messageTypeHandlerFactory.create(data.message.msgType)(data);
                };

                webSocket.onclose = function(e) {
                    console.log(e);
                };

                webSocket.onerror = function(e) {
                    console.log(e);
                };

                $scope.authenticate = function() {
                    webSocket.send(angular.toJson({
                        msgType: 'AUTH',
                        body: {
                            username: $scope.username,
                            password: $scope.password
                        }
                    }));
                };

                $scope.keyHandler = function(event, handler) {
                    if (event.keyCode == 13) {
                        handler();
                    }
                };

                $scope.sendMessage = function() {
                    webSocket.send(angular.toJson({
                        msgType: 'TEXT',
                        body: {
                            text: $scope.message,
                            username: ''
                        }
                    }));
                    $scope.message = '';
                };

                $scope.messageLog = function() {
                    return $sce.trustAsHtml($scope.messageStorage.join('<br />'));
                };

                $scope.concatParts = function() {
                    return concatParts.apply(arguments);
                };

                function concatParts() {
                    var args = arguments;
                    var length = args.length;
                    var result = [];
                    for (var i = 0; i < length; ++i) {
                        var arg = args[i];
                        if (arg)
                            result[result.length] = arg;
                    }

                    return result.join('');
                }

                function buildMessage(message) {
                    var userClass = 'message-username';
                    if (message.username == $scope.username)
                        userClass = 'message-my-username';
                    else if (message.username == 'system')
                        userClass = 'message-system-username';

                    return concatParts(
                        '<span class="', userClass, '">',
                        message.username,
                        '</span><span class="message-time">',
                        '[', formatTime(new Date()), ']</span>: ',
                        '<span class="message-text">', message.text, '</span>'
                    );

                    function formatTime(date) {
                        return concatParts(
                            date.getHours(),
                            ':',
                            date.getMinutes(),
                            ':',
                            date.getSeconds()
                        );
                    }
                }

                function pushMessage(message) {
                    var storage = $scope.messageStorage
                    storage[storage.length] = message;
                }

                var messageTypeHandlerFactory = {
                    create: function(type) {
                        switch (type) {
                            case "AUTH":
                                return authMessageHandler;
                            case "TEXT":
                                return textMessageHandler;
                        }

                        function authMessageHandler(data) {
                            if (data.success) {
                                $scope.authenticated = true;
                                $scope.$apply();
                            }
                        }

                        function textMessageHandler(data) {
                            if (data.success) {
                                pushMessage(buildMessage(data.message.body));
                                $scope.$apply();
                            }
                        }
                    }
                };
            }]
        }
    });