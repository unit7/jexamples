package com.unit7.conversa.app.server.message.format

import com.unit7.conversa.app.pattern.Converter
import spray.json._
import com.unit7.conversa.app.server.message.{Answer, Message}
import com.unit7.conversa.app.server.message.format.JsonProtocol._

/**
 * Created by breezzo on 13.09.15.
 * <p/>
 * Converter of type {@link com.unit7.conversa.app.server.message.Message}
 */
class JsonMessageConverter extends Converter[Message, String] {

  def convertTarget(message: String) = {
    message.parseJson.convertTo[Message]
  }

  def convertSource(message: Message) = {
    message.toJson.toString()
  }
}

object JsonMessageConverter {
  def apply() = {
    new JsonMessageConverter
  }
}

class JsonAnswerConverter extends Converter[Answer, String] {
  def convertSource(answer: Answer) = {
    answer.toJson.toString()
  }

  def convertTarget(answer: String) = {
    answer.parseJson.convertTo[Answer]
  }
}

object JsonAnswerConverter {
  def apply() = {
    new JsonAnswerConverter
  }
}