package com.unit7.conversa.app.server.message.format

import com.unit7.conversa.app.pattern.{Converter => BaseConverter}

/**
 * Created by breezzo on 13.09.15.
 */
object ConverterFactory {

  import ConverterType._

  def create[S, T](converterType: ConverterType.Value): BaseConverter[S, T] = {
    converterType match {
      case MESSAGE => JsonMessageConverter.apply().asInstanceOf[BaseConverter[S, T]]
      case ANSWER => JsonAnswerConverter.apply().asInstanceOf[BaseConverter[S, T]]
      case x: Any =>
        throw new IllegalArgumentException("Could not find converter for type: " + x.getClass.getCanonicalName)
    }
  }
}

object ConverterType extends Enumeration {
  val MESSAGE = Value
  val ANSWER = Value
}