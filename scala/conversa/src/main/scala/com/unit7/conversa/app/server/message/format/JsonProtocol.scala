package com.unit7.conversa.app.server.message.format

import com.unit7.conversa.app.server.message._
import spray.json._

object JsonProtocol extends DefaultJsonProtocol {

  implicit def messageTypeFormat = new JsonFormat[MessageType.Value] {
    override def read(json: JsValue): MessageType.Value = json match {
      case JsString(x) => MessageType.withName(x)
      case x => deserializationError("Expected string for MessageType, but got " + x)
    }

    override def write(obj: MessageType.Value): JsValue = obj.toString.toJson
  }

  implicit val textMessageBodyFormat = jsonFormat2(TextMessageBody.apply)
  implicit val authMessageBodyFormat = jsonFormat2(AuthMessageBody.apply)
  implicit val noMessageBodyFormat = jsonFormat0(NoMessageBody.apply)

  implicit def messageBodyFormat = new JsonFormat[MessageBody] {
    override def read(json: JsValue): MessageBody = {
      val jsonObj = json.asJsObject
      val textPresent = jsonObj.fields.contains("text")
      val usernamePresent = jsonObj.fields.contains("username")

      if (textPresent) return json.convertTo[TextMessageBody]
      if (usernamePresent) return json.convertTo[AuthMessageBody]

      deserializationError("Unknown json object " + json)
    }

    override def write(obj: MessageBody): JsValue = obj match {
      case e: TextMessageBody => e.toJson
      case e: AuthMessageBody => e.toJson
      case e: NoMessageBody => e.toJson
      case x => serializationError("Unknown message body to serialize " + x)
    }
  }

  implicit val messageFormat = jsonFormat2(Message.apply)
  implicit val answerFormat = jsonFormat2(Answer.apply)
}