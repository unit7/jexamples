package com.unit7.conversa.app.info

/**
 * Created by breezzo on 05.09.15.
 */
class Version {
  val major = 0
  val minor = 1
  val build = 1

  def version: String = {
    StringBuilder.newBuilder
      .append(major)
      .append(".")
      .append(minor)
      .append(".")
      .append(build)
      .toString()
  }

  override def toString: String = {
    StringBuilder.newBuilder
      .append("Version [ major: ")
      .append(major)
      .append(", minor: ")
      .append(minor)
      .append(", build: ")
      .append(build)
      .append(" ]")
      .toString()
  }
}
