package com.unit7.conversa.app

import java.util.concurrent.TimeUnit

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.io.IO
import com.unit7.conversa.app.server.{UserService, WebSocketServer}
import spray.can.Http
import spray.can.server.{ServerSettings, UHttp}
import spray.routing.SimpleRoutingApp

import scala.concurrent.duration.Duration
import scala.io.StdIn

/**
 * Created by breezzo on 01.09.15.
 */
object ConversaApplication extends App with SimpleRoutingApp {

  implicit val system = ActorSystem("conversa")

  val userService = system.actorOf(Props(classOf[UserService]), "userService")

//  startServer(interface = "localhost", port = 8080) {
//    pathPrefix("conversa" / "static") {
//      getFromDirectory("/home/breezzo/sources/Idea/scala/conversa/src/main/resources/static")
//    }
//  }

  println("Http server started. Start websocket server...")

  val wsServer: ActorRef = system.actorOf(Props(classOf[WebSocketServer]), "websocket");

  IO(UHttp) ! Http.Bind(wsServer, interface = "localhost", port = 8080);

  StdIn.readLine("Server completely started. Hit ENTER to stop...")

  system.shutdown();
  system.awaitTermination();
}
