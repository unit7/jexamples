package com.unit7.conversa.app.server

import java.text.SimpleDateFormat

import akka.actor._
import com.unit7.conversa.app.server.message._
import com.unit7.conversa.app.server.message.format.{ConverterFactory, ConverterType}
import spray.can.Http
import spray.can.websocket.frame.TextFrame
import spray.can.websocket.{FrameCommandFailed, WebSocketServerWorker}
import spray.http.HttpRequest
import spray.routing.HttpServiceActor

class WebSocketServer extends Actor with ActorLogging {
  override def receive: Receive = {
    case Http.Connected(remoteAddress, localAddress) =>
      log.info("client connected")
      val serverConnection = sender()
      val conn = context.actorOf(WebSocketWorker.props(serverConnection))
      serverConnection ! Http.Register(conn)
  }
}

object WebSocketWorker {
  def props(serverConnection: ActorRef) = Props(classOf[WebSocketWorker], serverConnection)
}

class WebSocketWorker(val serverConnection: ActorRef) extends HttpServiceActor with WebSocketServerWorker {
  override def receive = handshaking orElse businessLogicNoUpgrade orElse closeLogic

  val userService = context.actorSelection("akka://conversa/user/userService")

  val messageConverter = ConverterFactory.create[Message, String](ConverterType.MESSAGE)

  def businessLogic: Receive = {

    case t: TextFrame =>
      userService ! MessageWrapper(parse(t), sender())

    case x: FrameCommandFailed =>
      log.error("frame command failed", x)

    case x: HttpRequest => // do something
  }

  def parse(frame: TextFrame): Message = {
    val rawString = frame.payload.utf8String
    log.debug("input frame: " + rawString)

    messageConverter.convertTarget(rawString)
  }

  def businessLogicNoUpgrade: Receive = {
    implicit val refFactory: ActorRefFactory = context
    runRoute {
      pathPrefix("conversa") {
        getFromDirectory("/home/breezzo/sources/Idea/scala/conversa/src/main/resources/")
      }
    }
  }
}

class UserService extends Actor with ActorLogging {
  private val users = collection.mutable.Map(User("system", "") -> self)
  private val connectionOnUsers = collection.mutable.Map[ActorRef, User](self -> User("system", ""))

  private val answerConverter = ConverterFactory.create[Answer, String](ConverterType.ANSWER)

  override def receive: Actor.Receive = {
    case m: MessageWrapper =>
      m.message.body match {
        case AuthMessageBody(username, password) =>
          var result = false
          val user = User(username, password)
          if (!users.contains(user)) {
            users.put(user, context.watch(m.connection))
            connectionOnUsers.put(m.connection, user)
            result = true
          }

          // сообщение об успешной авторизации
          val answer = answerConverter.convertSource(Answer(MessageFactory.create(MessageType.AUTH), true))

          m.connection ! TextFrame(answer)

          // сообщение о входе в чат всем пользователям
          val answerMessageText = username + " вошел в чат " + new SimpleDateFormat("dd.MM.yyyy HH:mm:ss:SSS").format(new java.util.Date())

          sendToUsers(Answer(MessageFactory.create(MessageType.TEXT, answerMessageText, "system"), true))

        case x: TextMessageBody =>
          val sendUser: User = connectionOnUsers(m.connection)
          sendToUsers(Answer(MessageFactory.create(MessageType.TEXT, x.text, sendUser.username), true))

        case _ =>
          log.error("unkown message body type")
      }

    case Terminated(ref) =>
      val user = connectionOnUsers(ref)
      users.remove(user)
      connectionOnUsers.remove(ref)

      val exitMessageText = user.username + " покинул чат " + new SimpleDateFormat("dd.MM.yyyy HH:mm:ss:SSS").format(new java.util.Date())

      sendToUsers(Answer(MessageFactory.create(MessageType.TEXT, exitMessageText, "system"), true))
  }

  def sendToUsers(answer: Answer): Unit = {
    users.foreach(p => {
      if (p._1.username != "system")
        p._2 ! TextFrame(answerConverter.convertSource(answer))
    })
  }
}

case class User(username: String, password: String)