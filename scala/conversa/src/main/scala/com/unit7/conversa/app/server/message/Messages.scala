package com.unit7.conversa.app.server.message

import akka.actor.ActorRef

/**
 * Created by breezzo on 09.09.15.
 */
object MessageType extends Enumeration {
  val EMPTY = Value
  val AUTH = Value
  val TEXT = Value
}

sealed trait MessageBody {
}

case class AuthMessageBody(username: String, password: String) extends MessageBody

case class TextMessageBody(text: String, username: String) extends MessageBody

case class NoMessageBody() extends MessageBody

case class Message(msgType: MessageType.Value, body: MessageBody)

case class MessageWrapper(message: Message, connection: ActorRef)

case class Answer(message: Message, success: Boolean)

object MessageFactory {

  import MessageType._

  def create(msgType: MessageType.Value, args: Any*): Message = {
    msgType match {
      case AUTH =>
        Message(AUTH,
          if (args.nonEmpty)
            AuthMessageBody(args(0).asInstanceOf[String], args(1).asInstanceOf[String])
          else
            NoMessageBody()
        )

      case TEXT =>
        Message(TEXT, TextMessageBody(args(0).asInstanceOf[String], args(1).asInstanceOf[String]))

      case _ => Message(EMPTY, NoMessageBody())
    }
  }
}