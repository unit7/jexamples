package com.unit7.conversa.app.pattern

/**
 * Created by breezzo on 13.09.15.
 */
trait Converter[S, T] {

  def convertSource(p: S): T

  def convertTarget(p: T): S
}