package com.unit7.chapter11.buffered

import java.io.{FileInputStream, FileOutputStream, File, InputStream}

import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfter
import org.scalatest.junit.JUnitRunner

/**
 * Created by breezzo on 26.07.15.
 *//*
@RunWith(classOf[JUnitRunner])
class BufferedStreamTest extends BeforeAndAfter {
  var stream: InputStream

  private val text = "simple text test";
  private val textSize = text.length

  private var tempFileName = ""

  before {
    val file = File.createTempFile("BufferedStreamTest", ".tmp")
    tempFileName = file.getAbsolutePath
    val writer = new FileOutputStream(file)

    writer.write(text.getBytes("UTF-8"))
    writer.close()

    stream = new FileInputStream(tempFileName) with BufferedStream
  }

  test ("Readed text should be equals written") {
    val array = new Array[Byte](1024)

    val readed = stream.read(array)

    assert(readed == textSize, "")
  }

  after {
    stream.close()

    new File(tempFileName).delete()
  }
}
*/