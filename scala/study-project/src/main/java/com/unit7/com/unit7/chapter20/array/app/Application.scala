package com.unit7.com.unit7.chapter20.array.app

import java.util.concurrent._

import akka.actor.{Props, ActorSystem}
import com.unit7.chapter20.array.actor.{TaskReducer, TaskComputer, TaskMapper}

import scala.io.StdIn

import java.util.Random

/**
 * Created by breezzo on 13.09.15.
 */
object Application extends App {

  val SIZE = 280000000
  val task = new Array[Double](SIZE)

  System.out.print("Task to computation [ ")

  val random = new Random()
  for (i <- 0 to SIZE - 1) {
    task(i) = random.nextInt(10)
    //System.out.print(task(i) + ", ")
  }

  val start = System.currentTimeMillis()

  var sum: Double = 0
  for (i <- 0 to task.length - 1) {
    sum *= task(i) /5
  }

  System.out.println(" ]: " + sum + " time: " + (System.currentTimeMillis() - start))

  actors()



  def actors(): Unit = {
    val system = ActorSystem("compute")

    val mapper = system.actorOf(Props(classOf[TaskMapper]), "mapper")
    val computer = system.actorOf(Props(classOf[TaskComputer]), "computer")
    val reducer = system.actorOf(Props(classOf[TaskReducer]), "reducer")

    mapper ! task
    StdIn.readLine("Task send to computation. Hit ENTER to stop...\n")

    system.shutdown()
    system.awaitTermination()
  }

  def threads(): Unit = {

    val executor = Executors.newFixedThreadPool(5)

    val taskSize = SIZE / 5

    val futures = new Array[Future[Double]](5)
    val latch = new CountDownLatch(5)

    val startTime = System.currentTimeMillis()

    for (i <- 1 to 4) {
      val start = (i - 1) * taskSize
      val end = start + taskSize
      futures(i - 1) = executor.submit(new TaskExecutor(task, start, end, latch))
    }

    futures(4) = executor.submit(new TaskExecutor(task, 4 * taskSize, task.length, latch))

    latch.await()

    var result = 0.0
    for (future <- futures) {
      result += future.get()
    }

    System.out.println("Execution complete. result " + result + ". Time: " + (System.currentTimeMillis() - startTime));

    executor.shutdown()
    executor.awaitTermination(10, TimeUnit.SECONDS)
  }

  class TaskExecutor(val task: Array[Double], val start: Int, val end: Int, val latch: CountDownLatch) extends Callable[Double] {

    override def call(): Double = {
      var result = 0.0
      for (i <- start to end - 1)
        result *= task(i) / 5

      latch.countDown()
      result
    }
  }
}