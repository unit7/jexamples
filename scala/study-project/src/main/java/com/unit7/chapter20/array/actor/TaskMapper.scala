package com.unit7.chapter20.array.actor

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.concurrent.TimeUnit

import akka.actor.{Props, Actor, ActorLogging}

/**
 * Created by breezzo on 13.09.15.
 */
class TaskMapper extends Actor with ActorLogging {

  private val actorComputer = context.actorSelection("akka://compute/user/computer")
  
  override def receive: Receive = {
    case x: Array[Double] =>
      map(x)
    case x: Any =>
      log.error("Error reduce parameter: " + x.getClass().getCanonicalName)
  }

  private val PIECE_SIZE = 5

  def map(array: Array[Double]) = {
    val (taskSize, lastTaskSize) = computeTaskSize(array.length)

    val taskId = System.nanoTime()

    log.info("Map task computation [ " + taskId + " ] at " + System.currentTimeMillis())

    for (i <- 1 to PIECE_SIZE - 1) {
      val start = (i - 1) * taskSize
      val end = start + taskSize
      delegateTask(UnitOfWork(array, start, end, taskId))
    }

    delegateTask(UnitOfWork(array, array.length - lastTaskSize, array.length, taskId))
  }

  def delegateTask(unitOfWork: UnitOfWork) {
    log.info("Task send to computation")

    val actorComputer = context.actorOf(Props(classOf[TaskComputer]))
    actorComputer ! unitOfWork
  }

  def computeTaskSize(size: Int) = {
    val taskSize = size / PIECE_SIZE
    val lastTaskSize = size - taskSize * PIECE_SIZE + taskSize

    (taskSize, lastTaskSize)
  }
}
