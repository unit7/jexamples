package com.unit7.chapter20.array.actor

/**
 * Created by breezzo on 13.09.15.
 */
case class UnitOfWork(work: Array[Double], start: Int, end: Int, id: Long)

case class WorkResult(result: Double, work: UnitOfWork)