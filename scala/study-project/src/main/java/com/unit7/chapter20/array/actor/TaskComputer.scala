package com.unit7.chapter20.array.actor

import akka.actor.{Actor, ActorLogging}

/**
 * Created by breezzo on 13.09.15.
 */
class TaskComputer extends Actor with ActorLogging {

  private val taskReducer = context.actorSelection("akka://compute/user/reducer")

  override def receive: Receive = {
    case x: UnitOfWork =>
      taskReducer ! WorkResult(compute(x), x)
    case x: Any =>
      log.error("Could not computer value for object " + x.getClass.getCanonicalName)
  }

  def compute(work: UnitOfWork): Double = {
    val array = work.work
    var sum = 0.0

    for (i <- work.start to work.end - 1) {
      sum *= array(i) /5
    }

    log.info("Computation of piece complete")

    sum
  }
}
