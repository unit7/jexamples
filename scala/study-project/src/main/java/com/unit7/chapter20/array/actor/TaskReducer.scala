package com.unit7.chapter20.array.actor

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import akka.actor.{Actor, ActorLogging}

/**
 * Created by breezzo on 13.09.15.
 */
class TaskReducer extends Actor with ActorLogging {

  private val resultMap = scala.collection.mutable.Map[Long, (Double, Int)]()

  override def receive: Receive = {
    case x: WorkResult =>
      reduce(x)
    case x: Any =>
      log.error("Could not reduce result of " + x.getClass.getCanonicalName)
  }

  def reduce(result: WorkResult): Unit = {
    log.info("Reduce piece of task")

    val workId = result.work.id
    val currentResult: (Double, Int) = resultMap.getOrElse(workId, (0.0, 0))

    resultMap.put(workId, (currentResult._1 + result.result, currentResult._2 + (result.work.end - result.work.start)))

    checkComplete(workId, result.work.work.length)
  }

  def checkComplete(workId: Long, totalSize: Int): Unit = {
    val currentResult: (Double, Int) = resultMap.getOrElse(workId, (0.0, 0))

    if (currentResult._2 == totalSize) {
      log.info("Computation complete for task [ " + workId + " ], result: " + currentResult._1 + " at " +
        System.currentTimeMillis())

      resultMap.remove(workId)
    }
  }
}
