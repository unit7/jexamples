package com.unit7.chapter11.buffered

import java.io.InputStream

/**
 * Created by breezzo on 26.07.15.
 */
trait BufferedStream extends InputStream {
  import BufferedStream._

  private var buffer = new Array[Byte](BUFFER_SIZE)
  private var bufferEndPointer = 0
  private var bufferStartPointer = 0

  override def reset(): Unit = super.reset()

  override def read(): Int = {
    if (bufferEndPointer - 1 <= bufferStartPointer) {
      readToBuffer()
    }

    if (bufferEndPointer == -1)
      return -1

    bufferStartPointer += 1
    buffer(bufferStartPointer - 1)
  }

  override def read(b: Array[Byte], off: Int, len: Int): Int = {
    if (b == null)
      throw new IllegalArgumentException("b should be not null")
    else if (off < 0 || len < 0 || len > b.length - off)
      throw new IndexOutOfBoundsException
    else if (len == 0)
      return 0

    var c = read()
    if (c == -1)
      return -1

    b(off) = c.asInstanceOf[Byte]

    var readed = 1

    for (i <- (off + 1) to len) {
      c = read()
      if (c == -1)
        return readed
      else
        b(i) = c.asInstanceOf[Byte]
    }

    readed
  }

  override def read(b: Array[Byte]): Int = {
    read(b, 0, b.length)
  }

  override def close(): Unit = {
    buffer = null
  }

  protected def readToBuffer(): Unit = {
    bufferEndPointer = super.read(buffer)
    bufferStartPointer = 0
  }
}

private object BufferedStream {
  val BUFFER_SIZE = 4096
}