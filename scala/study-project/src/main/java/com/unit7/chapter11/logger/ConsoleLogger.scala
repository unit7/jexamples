package com.unit7.chapter11.logger

/**
 * Created by breezzo on 21.07.15.
 */
class ConsoleLogger extends Logger {
  protected override def log(str: String, level: Logger.Value): Unit = {
    println(level + " " + str)
  }
}

object ConsoleLoggerApp extends App {
  val logger = new ConsoleLogger with DateTimeLogger with CryptoLogger

  logger.info("level info check")
  logger.debug("level debug check")
  logger.debug("all works fine")
  logger.info("good bye")
}