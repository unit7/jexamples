package com.unit7.chapter11.listener

import java.awt.Point
import java.beans.{PropertyChangeEvent, PropertyChangeListener, PropertyChangeSupport}

/**
 * Created by breezzo on 26.07.15.
 */
object PointListenerApp extends App {
  val point = new Point() with PropertyChangeSupportTrait {
    override protected val listenerSupport: PropertyChangeSupport = new PropertyChangeSupport()
  }
}

trait PropertyChangeSupportTrait {
  protected val listenerSupport: PropertyChangeSupport

  def hasListeners(propertyName: String): Boolean = {
    listenerSupport.hasListeners(propertyName)
  }

  def getPropertyChangeListeners: Array[PropertyChangeListener] = {
    listenerSupport.getPropertyChangeListeners
  }

  def getPropertyChangeListeners(propertyName: String): Array[PropertyChangeListener] = {
    listenerSupport.getPropertyChangeListeners(propertyName)
  }

  def removePropertyChangeListener(listener: PropertyChangeListener): Unit = {
    listenerSupport.removePropertyChangeListener(listener)
  }

  def removePropertyChangeListener(propertyName: String, listener: PropertyChangeListener): Unit = {
    listenerSupport.removePropertyChangeListener(propertyName, listener)
  }

  def firePropertyChange(propertyName: String, oldValue: scala.Any, newValue: scala.Any): Unit = {
    listenerSupport.firePropertyChange(propertyName, oldValue, newValue)
  }

  def firePropertyChange(propertyName: String, oldValue: Int, newValue: Int): Unit = {
    listenerSupport.firePropertyChange(propertyName, oldValue, newValue)
  }

  def firePropertyChange(propertyName: String, oldValue: Boolean, newValue: Boolean): Unit = {
    listenerSupport.firePropertyChange(propertyName, oldValue, newValue)
  }

  def firePropertyChange(event: PropertyChangeEvent): Unit = {
    listenerSupport.firePropertyChange(event)
  }

  def fireIndexedPropertyChange(propertyName: String, index: Int, oldValue: scala.Any, newValue: scala.Any): Unit = {
    listenerSupport.fireIndexedPropertyChange(propertyName, index, oldValue, newValue)
  }

  def fireIndexedPropertyChange(propertyName: String, index: Int, oldValue: Int, newValue: Int): Unit = {
    listenerSupport.fireIndexedPropertyChange(propertyName, index, oldValue, newValue)
  }

  def fireIndexedPropertyChange(propertyName: String, index: Int, oldValue: Boolean, newValue: Boolean): Unit = {
    listenerSupport.fireIndexedPropertyChange(propertyName, index, oldValue, newValue)
  }

  def addPropertyChangeListener(listener: PropertyChangeListener): Unit = {
    listenerSupport.addPropertyChangeListener(listener)
  }

  def addPropertyChangeListener(propertyName: String, listener: PropertyChangeListener): Unit = {
    listenerSupport.addPropertyChangeListener(propertyName, listener)
  }
}