package com.unit7.chapter11.logger

/**
 * Created by breezzo on 21.07.15.
 */
trait CryptoLogger extends Logger {
  def key = 3

  def cryptString(str: String): String = {
    for (ch <- str) yield cryptChar(ch)
  }

  private def cryptChar(ch: Char): Char = {
    (ch + key).asInstanceOf[Char]
  }

  protected override def log(str: String, level: Logger.Value): Unit = {
    super.log(cryptString(str), level)
  }
}
