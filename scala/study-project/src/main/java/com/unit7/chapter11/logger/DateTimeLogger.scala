package com.unit7.chapter11.logger

import java.text.SimpleDateFormat
import java.util.Date

/**
 * Created by breezzo on 21.07.15.
 */
trait DateTimeLogger extends Logger {
  protected override def log(str: String, level: Logger.Value): Unit = {
    val date: Date = new Date();
    val formatter = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss.ms")

    val formatted = formatter.format(date) + " " + str

    super.log(formatted, level)
  }
}
