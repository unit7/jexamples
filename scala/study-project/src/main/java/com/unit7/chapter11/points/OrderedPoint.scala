package com.unit7.chapter11.points

import java.awt.Point

/**
 * Created by breezzo on 20.07.15.
 */
class OrderedPoint(x: Int, y: Int) extends Point(x, y) with Ordered[Point] {
  override def compare(that: Point): Int = if (that.x == x) y - that.y else x - that.x
}

object Application extends App {
  val pointArray: Array[OrderedPoint] = new Array[OrderedPoint](3)

  pointArray(0) = new OrderedPoint(5, 2)
  pointArray(1) = new OrderedPoint(5, 3)
  pointArray(2) = new OrderedPoint(1, 8)

  val sorted = pointArray.sortWith((x, y) => x.compare(y) < 0)

  for { p <- sorted } println(p)
}