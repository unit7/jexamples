package com.unit7.chapter11.logger

/**
 * Created by breezzo on 21.07.15.
 */
trait Logger {
  import Logger._

  def info(str: String): Unit = {
    log(str, INFO)
  }

  def debug(str: String): Unit = {
    log(str, DEBUG)
  }

  protected def log(str: String, level: Logger.Value) { }
}

object Logger extends Enumeration {
  val INFO = Value
  val DEBUG = Value
}
