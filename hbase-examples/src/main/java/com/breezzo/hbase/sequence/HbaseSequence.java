package com.breezzo.hbase.sequence;

import com.breezzo.hbase.employee.Schema;
import com.breezzo.hbase.tools.ConfUtils;
import com.google.common.collect.Iterables;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Durability;
import org.apache.hadoop.hbase.client.Table;

import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

/**
 * Created by breezzo on 08.01.17.
 */
public class HbaseSequence implements Sequence<Long> {

    private Set<Long> generate(KeySpace keySpace, long amount) throws IOException {
        Configuration conf = ConfUtils.defaultConf();
        // TODO connection pooling?
        try (Connection conn = ConnectionFactory.createConnection(conf)) {
            Table table = conn.getTable(TableName.valueOf(Schema.Table.PK_SEQUENCE.getName()));
            long keys = table.incrementColumnValue(
                    keySpace.getRowKey(),
                    Schema.ColumnFamily.KEYS.getQualifier(),
                    Schema.Column.ID.getQualifier(),
                    amount,
                    Durability.SYNC_WAL
            );

            return LongStream.range(keys - amount + 1, keys + 1).boxed().collect(Collectors.toSet());
        }
    }

    @Override
    public Long next(KeySpace keySpace) {
        return Iterables.getOnlyElement(next(keySpace, 1));
    }

    @Override
    public Set<Long> next(KeySpace keySpace, long amount) {
        try {
            return generate(keySpace, amount);
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
}
