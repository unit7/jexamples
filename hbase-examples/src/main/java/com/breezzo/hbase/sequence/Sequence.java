package com.breezzo.hbase.sequence;

import org.apache.hadoop.hbase.util.Bytes;

import java.util.Collection;
import java.util.Set;

/**
 * Created by breezzo on 08.01.17.
 */
public interface Sequence<T> {
    T next(KeySpace keySpace);

    Set<T> next(KeySpace keySpace, long amount);

    enum KeySpace {
        EMPLOYEE(Bytes.toBytes(1L));

        private final byte[] rowKey;

        KeySpace(byte[] rowKey) {
            this.rowKey = rowKey;
        }

        public byte[] getRowKey() {
            return rowKey;
        }
    }
}
