package com.breezzo.hbase.sequence;

import com.google.common.collect.Iterables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by breezzo on 08.01.17.
 */
public class PrefetchSequence<T> implements Sequence<T> {
    private static final Logger logger = LoggerFactory.getLogger(PrefetchSequence.class);

    // TODO missing ids
    private static final int DEFAULT_PREFETCH_SIZE = 1000;

    private final int prefetchSize;
    private final Sequence<T> delegate;
    private final Deque<T> cache;

    public PrefetchSequence(Sequence<T> delegate) {
        this(DEFAULT_PREFETCH_SIZE, delegate);
    }

    public PrefetchSequence(int prefetchSize, Sequence<T> delegate) {
        this.delegate = delegate;
        this.prefetchSize = prefetchSize;
        this.cache = new ArrayDeque<>(prefetchSize);
    }

    private void fetchCache(KeySpace keySpace, long minAmount) {
        long needFetch = Math.max(minAmount, prefetchSize);
        logger.debug("fetching next {} ids for {} keySpace", needFetch, keySpace);
        cache.addAll(delegate.next(keySpace, needFetch));
        logger.debug("after fetch cache.size {}", cache.size());
    }

    private Set<T> fetch(KeySpace keySpace, long amount) {
        if (cache.size() < amount) {
            fetchCache(keySpace, cache.size() - amount);
        }

        // TODO reduce objects creation
        Set<T> result = new HashSet<>();
        for (int i = 0; i < amount; ++i) {
            result.add(cache.pollFirst());
        }
        return result;
    }

    @Override
    public T next(KeySpace keySpace) {
        if (cache.isEmpty()) {
            return Iterables.getOnlyElement(fetch(keySpace, 1));
        }
        return cache.pollFirst();
    }

    @Override
    public Set<T> next(KeySpace keySpace, long amount) {
        return fetch(keySpace, amount);
    }
}
