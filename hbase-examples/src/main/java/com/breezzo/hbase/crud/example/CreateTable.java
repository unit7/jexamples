package com.breezzo.hbase.crud.example;

import com.breezzo.hbase.tools.ConfUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;

import java.io.IOException;

/**
 * Created by breezzo on 04.09.16.
 */
public class CreateTable {
    public static void main(String[] args) throws IOException {
        Configuration conf = ConfUtils.defaultConf();
        try (Connection connection = ConnectionFactory.createConnection(conf)) {
            Admin admin = connection.getAdmin();

            HTableDescriptor table = new HTableDescriptor(TableName.valueOf("emp"));

            table.addFamily(new HColumnDescriptor("personal"));
            table.addFamily(new HColumnDescriptor("professional"));

            admin.createTable(table);
        }

        System.out.println("table created");
    }
}
