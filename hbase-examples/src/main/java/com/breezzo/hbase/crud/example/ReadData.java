package com.breezzo.hbase.crud.example;

import com.breezzo.hbase.tools.ConfUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;

import static org.apache.hadoop.hbase.util.Bytes.toBytes;

/**
 * Created by breezzo on 04.09.16.
 */
public class ReadData {
    public static void main(String[] args) throws IOException {
        Configuration conf = ConfUtils.defaultConf();
        try (Connection conn = ConnectionFactory.createConnection(conf)) {
            Table table = conn.getTable(TableName.valueOf("emp"));

            Get get = new Get(toBytes("r1"));

            get.addColumn(toBytes("personal"), toBytes("name"));

            Result result = table.get(get);
            byte[] nameVal = result.getValue(toBytes("personal"), toBytes("name"));

            System.out.println("retrieved data: " + Bytes.toString(nameVal));
        }
    }
}
