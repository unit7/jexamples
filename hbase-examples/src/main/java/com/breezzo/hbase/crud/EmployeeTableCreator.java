package com.breezzo.hbase.crud;

import com.breezzo.hbase.employee.Schema;
import com.breezzo.hbase.tools.ConfUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.regionserver.BloomType;

import java.io.IOException;

/**
 * Created by breezzo on 08.01.17.
 */
public class EmployeeTableCreator {
    public static void main(String[] args) throws IOException {
        Configuration conf = ConfUtils.defaultConf();
        try (Connection conn = ConnectionFactory.createConnection(conf)) {
            Admin admin = conn.getAdmin();

            HTableDescriptor table = new HTableDescriptor(TableName.valueOf(Schema.Table.EMPLOYEE.getName()));

            table.addFamily(new HColumnDescriptor(Schema.ColumnFamily.WORK.getQualifier()));
            table.addFamily(new HColumnDescriptor(Schema.ColumnFamily.PERSONAL.getQualifier()));
            table.addFamily(new HColumnDescriptor(Schema.ColumnFamily.INFLATED_SALARY.getQualifier()));

            if (!admin.tableExists(table.getTableName())) {
                admin.createTable(table);
            }

            HTableDescriptor seqTable = new HTableDescriptor(TableName.valueOf(Schema.Table.PK_SEQUENCE.getName()));

            HColumnDescriptor keyColumnDesc = new HColumnDescriptor(Schema.ColumnFamily.KEYS.getQualifier());
            keyColumnDesc.setBloomFilterType(BloomType.NONE);
            keyColumnDesc.setInMemory(true);
            seqTable.addFamily(keyColumnDesc);

            if (!admin.tableExists(seqTable.getTableName())) {
                admin.createTable(seqTable);
            }
        }
    }
}
