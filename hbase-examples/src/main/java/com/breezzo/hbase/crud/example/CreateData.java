package com.breezzo.hbase.crud.example;

import com.breezzo.hbase.tools.ConfUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Table;

import java.io.IOException;

import static org.apache.hadoop.hbase.util.Bytes.toBytes;

/**
 * Created by breezzo on 04.09.16.
 */
public class CreateData {
    public static void main(String[] args) throws IOException {
        Configuration conf = ConfUtils.defaultConf();
        try (Connection conn = ConnectionFactory.createConnection(conf)) {

            Table table = conn.getTable(TableName.valueOf("emp"));

            Put put = new Put(toBytes("r1"));
            put.addColumn(toBytes("personal"), toBytes("name"), toBytes("vlad"));
            put.addColumn(toBytes("professional"), toBytes("designation"), toBytes("programmer"));
            put.addColumn(toBytes("professional"), toBytes("salary"), toBytes("120000"));

            table.put(put);

            System.out.println("data created");

            table.close();
        }
    }
}
