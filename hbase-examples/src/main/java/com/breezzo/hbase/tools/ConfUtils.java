package com.breezzo.hbase.tools;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;

/**
 * Created by breezzo on 07.01.17.
 */
public class ConfUtils {
    public static Configuration defaultConf() {
        Configuration conf = HBaseConfiguration.create();
        conf.addResource(new Path("/etc/hadoop/conf/core-site.xml"));
        return conf;
    }
}
