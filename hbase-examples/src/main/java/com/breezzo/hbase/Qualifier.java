package com.breezzo.hbase;

/**
 * Created by breezzo on 08.01.17.
 */
public interface Qualifier {
    byte[] getQualifier();
}
