package com.breezzo.hbase.employee.mapreduce;

import com.breezzo.hbase.employee.proto.EmployeeSchema;
import com.breezzo.hbase.sequence.HbaseSequence;
import com.breezzo.hbase.sequence.PrefetchSequence;
import com.breezzo.hbase.sequence.Sequence;
import com.breezzo.hbase.tools.ConfUtils;
import com.google.common.collect.Iterables;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.mapreduce.TableReducer;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static com.breezzo.hbase.employee.Schema.*;
import static org.apache.hadoop.hbase.util.Bytes.toBytes;

/**
 * Created by breezzo on 08.01.17.
 */
public class HdfsToHbaseLoader extends Configured implements Tool {
    public static void main(String[] args) throws Exception {
        System.exit(
                ToolRunner.run(ConfUtils.defaultConf(), new HdfsToHbaseLoader(), new String[] {
                        "/employee/hr/input/hr.csv"
                })
        );
    }

    @Override
    public int run(String[] args) throws Exception {
        Configuration conf = getConf();
        Job job = Job.getInstance(conf, HdfsToHbaseLoader.class.getSimpleName());

        job.setJarByClass(HdfsToHbaseLoader.class);

        job.setMapperClass(HdfsMapper.class);
        job.setInputFormatClass(TextInputFormat.class);
        job.setMapOutputKeyClass(ImmutableBytesWritable.class);
        job.setMapOutputValueClass(ImmutableBytesWritable.class);

        TableMapReduceUtil.initTableReducerJob(Table.EMPLOYEE.getName(), HbaseReducer.class, job);

        FileInputFormat.setInputPaths(job, args[0]);

        return job.waitForCompletion(true) ? 0 : -1;
    }

    private enum CounterType {
        CREATED_EMPLOYEE_INFO,
        SAVED_EMPLOYEE_INFO,
        SAVE_TIME_MS,
        TIME_PER_REDUCE_MS
    }

    private static class HdfsMapper
            extends Mapper<LongWritable, Text, ImmutableBytesWritable, ImmutableBytesWritable> {

        private Counter counter;
        private Sequence<Long> sequence = new PrefetchSequence<>(new HbaseSequence());

        @Override
        protected void setup(Context context) throws IOException, InterruptedException {
             counter = context.getCounter(CounterType.CREATED_EMPLOYEE_INFO);
        }

        @Override
        protected void map(LongWritable key,
                           Text value,
                           Context context) throws IOException, InterruptedException {

            String[] employeeInfos = value.toString().split(",");

            EmployeeSchema.WorkInfo workInfo = EmployeeSchema.WorkInfo.newBuilder()
                    .setSatisfactionLevel(Double.parseDouble(employeeInfos[0]))
                    .setLastEvaluation(Double.parseDouble(employeeInfos[1]))
                    .setProjectCount(Integer.parseInt(employeeInfos[2]))
                    .setAvgMonthlyWorkHours(Integer.parseInt(employeeInfos[3]))
                    .setCompanySpentYears(Integer.parseInt(employeeInfos[4]))
                    .setWorkAccident(Integer.parseInt(employeeInfos[5]) != 0)
                    .setLeftCompany(Integer.parseInt(employeeInfos[6]) != 0)
                    .setLastFiveYearsPromotion(Integer.parseInt(employeeInfos[7]) != 0)
                    .setDepartment(employeeInfos[8])
                    .setSalary(EmployeeSchema.WorkInfo.SalaryType.valueOf(employeeInfos[9].toUpperCase()))
                    .build();

            byte[] modelValue = workInfo.toByteArray();
            Long id = sequence.next(Sequence.KeySpace.EMPLOYEE);
            context.write(
                    new ImmutableBytesWritable(toBytes(id)), new ImmutableBytesWritable(modelValue)
            );

            counter.increment(1);
        }
    }

    private static class HbaseReducer
            extends TableReducer <ImmutableBytesWritable, ImmutableBytesWritable, ImmutableBytesWritable> {

        private Counter counter;
        private Counter saveTimeCounter;
        private Counter timePerReduceCounter;

        private long reduceAllTime = 0;
        private int reduceCount = 0;

        @Override
        protected void setup(Context context) throws IOException, InterruptedException {
            counter = context.getCounter(CounterType.SAVED_EMPLOYEE_INFO);
            saveTimeCounter = context.getCounter(CounterType.SAVE_TIME_MS);
            timePerReduceCounter = context.getCounter(CounterType.TIME_PER_REDUCE_MS);

            saveTimeCounter.setValue(System.currentTimeMillis());
        }

        @Override
        protected void reduce(ImmutableBytesWritable key,
                              Iterable<ImmutableBytesWritable> values,
                              Context context) throws IOException, InterruptedException {

            long start = System.currentTimeMillis();

            ImmutableBytesWritable model = Iterables.getOnlyElement(values);

            Put put = new Put(key.get());
            put.addColumn(
                    ColumnFamily.WORK.getQualifier(),
                    Column.MODEL.getQualifier(),
                    model.get()
            );
            context.write(key, put);

            counter.increment(1);

            reduceAllTime += System.currentTimeMillis() - start;
            ++reduceCount;
        }

        @Override
        protected void cleanup(Context context) throws IOException, InterruptedException {
            saveTimeCounter.setValue(System.currentTimeMillis() - saveTimeCounter.getValue());
            timePerReduceCounter.setValue(reduceAllTime / reduceCount);
        }
    }
}
