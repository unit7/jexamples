package com.breezzo.hbase.employee.mapreduce;

import com.breezzo.hbase.employee.Schema;
import com.breezzo.hbase.employee.proto.EmployeeSchema;
import com.breezzo.hbase.tools.ConfUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;

/**
 * Created by breezzo on 08.01.17.
 */
public class InflatedSalaryJobStats extends Configured implements Tool {
    public static void main(String[] args) throws Exception {
        System.exit(ToolRunner.
                run(ConfUtils.defaultConf(), new InflatedSalaryJobStats(), args));
    }

    @Override
    public int run(String[] args) throws Exception {
        Configuration conf = getConf();
        Job job = Job.getInstance(conf, InflatedSalaryJobStats.class.getSimpleName());

        Scan scan = new Scan();
        scan.setCacheBlocks(false);
        scan.setCaching(500);
        scan.addColumn(Schema.ColumnFamily.WORK.getQualifier(), Schema.Column.MODEL.getQualifier());

        TableMapReduceUtil.initTableMapperJob(
                Schema.Table.EMPLOYEE.getName(),
                scan,
                EmployeeMapper.class,
                null,
                Put.class,
                job,
                true
        );

        TableMapReduceUtil.initTableReducerJob(Schema.Table.EMPLOYEE.getName(), null, job);

        return job.waitForCompletion(true) ? 0 : -1;
    }

    private enum JobCounter {
        PROCESSED_MODELS
    }

    private static class EmployeeMapper
            extends TableMapper<ImmutableBytesWritable, Put> {

        private Counter counter;

        @Override
        protected void setup(Context context) throws IOException, InterruptedException {
            counter = context.getCounter(JobCounter.PROCESSED_MODELS);
        }

        @Override
        protected void map(ImmutableBytesWritable key,
                           Result value, Context context) throws IOException, InterruptedException {

            byte[] modelValue =
                    value.getValue(Schema.ColumnFamily.WORK.getQualifier(), Schema.Column.MODEL.getQualifier());
            EmployeeSchema.WorkInfo model = EmployeeSchema.WorkInfo.parseFrom(modelValue);

            if (isSuitable(model)) {
                byte[] inflatedSalaryModelBytes = EmployeeSchema.InflatedSalaryEmployee.newBuilder()
                        .setDepartment(model.getDepartment())
                        .setAvgMonthlyWorkHourse(model.getAvgMonthlyWorkHours())
                        .setLastEvaluation(model.getLastEvaluation())
                        .setSatisfactionLevel(model.getSatisfactionLevel())
                        .setProjectCount(model.getProjectCount())
                        .build()
                        .toByteArray();

                Put put = new Put(key.get());
                put.addColumn(Schema.ColumnFamily.INFLATED_SALARY.getQualifier(),
                        Schema.Column.MODEL.getQualifier(),
                        inflatedSalaryModelBytes);
                context.write(key, put);
                counter.increment(1);
            }
        }

        private boolean isSuitable(EmployeeSchema.WorkInfo employee) {
            return !employee.getLeftCompany() &&
                    employee.getSalary() == EmployeeSchema.WorkInfo.SalaryType.HIGH &&
                    employee.getWorkAccident() &&
                    employee.getLastEvaluation() < 0.5 &&
                    employee.getAvgMonthlyWorkHours() / 23 < 8;
        }
    }
}
