package com.breezzo.hbase.employee;

import com.breezzo.hbase.Qualifier;
import org.apache.hadoop.hbase.util.Bytes;

/**
 * Created by breezzo on 08.01.17.
 */
public class Schema {

    public enum ColumnFamily implements Qualifier {
        PERSONAL("p"),
        WORK("w"),
        KEYS("k"),
        INFLATED_SALARY("is");

        private final byte[] qualifier;

        ColumnFamily(String name) {
            this.qualifier = Bytes.toBytes(name);
        }

        @Override
        public byte[] getQualifier() {
            return qualifier;
        }
    }

    public enum Column implements Qualifier {
        MODEL("m"),
        ID("i");

        private final byte[] qualifier;

        Column(String qualifier) {
            this.qualifier = Bytes.toBytes(qualifier);
        }

        @Override
        public byte[] getQualifier() {
            return qualifier;
        }
    }

    public enum Table {
        EMPLOYEE("employee"),
        PK_SEQUENCE("pk_sequence");

        private final String name;

        Table(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }
}
