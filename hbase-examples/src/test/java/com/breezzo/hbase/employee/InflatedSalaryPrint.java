package com.breezzo.hbase.employee;

import com.breezzo.hbase.employee.proto.EmployeeSchema;
import com.breezzo.hbase.tools.ConfUtils;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;

import java.io.IOException;

/**
 * Created by breezzo on 08.01.17.
 */
public class InflatedSalaryPrint {

    public static void main(String[] args) throws IOException {
        try (Connection conn = ConnectionFactory.createConnection(ConfUtils.defaultConf());
             Table table = conn.getTable(TableName.valueOf(Schema.Table.EMPLOYEE.getName()))) {

            ResultScanner scanner = table.getScanner(Schema.ColumnFamily.INFLATED_SALARY.getQualifier());
            Result result;
            while ((result = scanner.next()) != null) {
                byte[] value = result.getValue(
                        Schema.ColumnFamily.INFLATED_SALARY.getQualifier(),
                        Schema.Column.MODEL.getQualifier());

                EmployeeSchema.InflatedSalaryEmployee info =
                        EmployeeSchema.InflatedSalaryEmployee.parseFrom(value);
                System.out.println(info);
            }
        }
    }
}
