package com.breezzo.hbase.sequence;

import org.junit.Assert;
import org.junit.Test;

import java.util.Iterator;
import java.util.Set;

/**
 * Created by breezzo on 08.01.17.
 */
public class SequenceTest {

    @Test
    public void generate() {
        HbaseSequence hbaseSequence = new HbaseSequence();
        Long current = hbaseSequence.next(Sequence.KeySpace.EMPLOYEE);
        long next = hbaseSequence.next(Sequence.KeySpace.EMPLOYEE);

        Assert.assertEquals(current + 1, next);

        current = next;
        final int requiredAmount = 3;
        Set<Long> nextThree = hbaseSequence.next(Sequence.KeySpace.EMPLOYEE, requiredAmount);

        Assert.assertEquals(requiredAmount, nextThree.size());

        Iterator<Long> it = nextThree.iterator();
        for (int i = 1; i <= requiredAmount; ++i) {
            next = it.next();
            Assert.assertEquals(current + i, next);
        }
    }
}
