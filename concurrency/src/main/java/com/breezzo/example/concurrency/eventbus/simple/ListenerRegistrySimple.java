package com.breezzo.example.concurrency.eventbus.simple;

import com.breezzo.example.concurrency.eventbus.EventListener;
import com.breezzo.example.concurrency.eventbus.ListenerRegistry;
import com.breezzo.example.concurrency.eventbus.ListenerRegistryDelegate;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by breezzo on 22.05.16.
 */
class ListenerRegistrySimple implements ListenerRegistry {

    private Map<Class<?>, Collection<EventListener>> listenerMap = new HashMap<>();

    private Collection<EventListener> getOrCreateListeners(Class<?> eventClass) {
        Collection<EventListener> listeners = this.listenerMap.get(eventClass);
        if (listeners == null) {
            listeners = new ArrayList<>();
            this.listenerMap.put(eventClass, listeners);
        }

        return listeners;
    }

    @Override
    public void addListener(@Nonnull EventListener eventListener, @Nonnull Class<?> listenerParamType) {
        getOrCreateListeners(listenerParamType).add(eventListener);
    }

    @Nonnull
    @Override
    public Collection<EventListener> findListeners(@Nonnull Object event) {
        return getOrCreateListeners(event.getClass());
    }
}
