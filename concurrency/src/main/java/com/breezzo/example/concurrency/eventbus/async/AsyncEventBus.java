package com.breezzo.example.concurrency.eventbus.async;

import com.breezzo.example.concurrency.eventbus.EventBus;
import com.breezzo.example.concurrency.eventbus.ListenerRegistryDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;

/**
 * Created by breezzo on 18.05.16.
 */
public class AsyncEventBus implements EventBus {

    private static final Logger logger = LoggerFactory.getLogger(AsyncEventBus.class);

    private static final int DEFAULT_QUEUE_SIZE = 512;

    private BlockingQueue<Object> eventQueue = new ArrayBlockingQueue<>(DEFAULT_QUEUE_SIZE);
    private ListenerRegistryDelegate listenerRegistry = ListenerRegistryDelegate.of(new ListenerRegistryAsync());
    private QueueHandler queueHandler;

    public AsyncEventBus() {
        this(null);
    }

    public AsyncEventBus(@Nullable ExecutorService executorService) {
        queueHandler = new QueueHandler(eventQueue, listenerRegistry);
        queueHandler.setExecutorService(executorService);
        queueHandler.start();
    }

    @Override
    public void post(Object event) {
        logger.trace("receive event {}", event);

        while (true) {
            try {
                eventQueue.put(event);
            } catch (InterruptedException e) {
                continue;
            }
            break;
        }
    }

    @Override
    public void registerListener(Object listener) {
        logger.trace("try register listener {}", listener);
        listenerRegistry.registerListener(listener);
    }
}
