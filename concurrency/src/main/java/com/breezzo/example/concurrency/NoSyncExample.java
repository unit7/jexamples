package com.breezzo.example.concurrency;

/**
 * Created by breezzo on 15.05.16.
 */
public class NoSyncExample {

    public static void main(String[] args) {
        new HappensBefore().run();
    }

    /**
     * В большинстве случаев этот код будет выполнять бесконечно.
     * Во-первых, sync не volatile, поэтому в основном потоке могут быть невалидные данные, т.е. без изменений второго потока.
     * Т.к. изменения второго потока могут находиться в локальном кэше процессора.
     * Во-вторых инструкция while(!sync); скорее всего будет преобразована в if (!sync) while(true);
     * Если же sync сделать volatile компилятор не сможет провести данную оптимизацию, т.к. переменная с
     * ключевым словом volatile должна всякий раз перечитываться из памяти.
     */
    private static class Infinite {
        private int data;
        private boolean sync;

        void run() {
            new Thread(() -> {
                data = 1;
                sync = true;
            }).start();

            while (!sync) ;

            System.out.println(data);
        }
    }

    /**
     * Код отработает корректно, т.к. sync имеет ключевое слово volatile. Кроме того это не повозляет
     * компилятору сделать reordering и перенести инструкцию data = 1 после sync = true. Т.к. операции с не volatile
     * переменными нельзя опускать ниже операций с volatile переменными, как это написано в оригинальном коде.
     * Т.е. happens-before на переменной sync гарантирует, что все операции предшествующие операции с volatile переменной
     * будут видны другому потоку.
     */
    private static class HappensBefore {
        private int data;
        private volatile boolean sync;

        void run() {
            new Thread(() -> {
                data = 1;
                sync = true;
            }).start();

            while (!sync);

            System.out.println(data);
        }
    }
}