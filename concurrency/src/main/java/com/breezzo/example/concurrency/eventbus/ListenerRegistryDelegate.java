package com.breezzo.example.concurrency.eventbus;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by breezzo on 22.05.16.
 */
public class ListenerRegistryDelegate implements ListenerRegistry {
    private static final Logger logger = LoggerFactory.getLogger(ListenerRegistryDelegate.class);

    private ListenerRegistry delegate;

    public ListenerRegistryDelegate(ListenerRegistry delegate) {
        this.delegate = delegate;
    }

    public static ListenerRegistryDelegate of(ListenerRegistry registry) {
        return new ListenerRegistryDelegate(registry);
    }

    @Override
    public void addListener(@Nonnull EventListener eventListener, @Nonnull Class<?> listenerParamType) {
        delegate.addListener(eventListener, listenerParamType);
    }

    @Nonnull
    @Override
    public Collection<EventListener> findListeners(@Nonnull Object event) {
        return delegate.findListeners(event);
    }

    public void registerListener(Object listener) {
        List<Method> listenerMethods = getAnnotatedMethods(listener.getClass());
        logger.trace("listener methods {}", listenerMethods);

        for (Method method : listenerMethods) {
            Class<?>[] listenerParamTypes = method.getParameterTypes();
            logger.trace("listener param types {}", listenerParamTypes);

            if (listenerParamTypes.length != 1) {
                throw new RuntimeException("method parameters length != 1");
            }

            Class<?> listenerParamType = listenerParamTypes[0];
            addListener(new Listener(method, listener), listenerParamType);
        }
    }

    private List<Method> getAnnotatedMethods(Class<?> c) {
        List<Method> result = new ArrayList<>();
        Method[] declaredMethods = c.getDeclaredMethods();

        for (Method method : declaredMethods) {
            Listened annotation = method.getAnnotation(Listened.class);
            if (annotation != null) {
                result.add(method);
            }
        }

        return result;
    }

    private static class Listener implements EventListener {
        Method m;
        Object target;

        Listener(Method m, Object target) {
            this.m = m;
            this.target = target;
        }

        @Override
        public void handle(Object event) {
            logger.debug("Handle event");

            try {
                m.invoke(target, event);
            } catch (Exception e) {
                throw new RuntimeException(e.getMessage(), e);
            }
        }
    }
}
