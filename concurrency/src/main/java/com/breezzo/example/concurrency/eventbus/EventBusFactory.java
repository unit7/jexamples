package com.breezzo.example.concurrency.eventbus;

import com.breezzo.example.concurrency.eventbus.async.AsyncEventBus;
import com.breezzo.example.concurrency.eventbus.simple.SimpleEventBus;

import java.util.concurrent.ExecutorService;

/**
 * Created by breezzo on 18.05.16.
 */
public class EventBusFactory {
    private static final EventBusFactory instance = new EventBusFactory();

    private EventBusFactory() {
    }

    public static EventBusFactory getInstance() {
        return instance;
    }

    public EventBus createDefault() {
        return new SimpleEventBus();
    }

    public EventBus createAsync() {
        return new AsyncEventBus();
    }

    public EventBus createAsync(ExecutorService executorService) {
        return new AsyncEventBus(executorService);
    }
}
