package com.breezzo.example.concurrency.eventbus;

import javax.annotation.Nonnull;
import java.util.Collection;

/**
 * Created by breezzo on 22.05.16.
 */
public interface ListenerRegistry {
    void addListener(@Nonnull EventListener eventListener, @Nonnull Class<?> listenerParamType);

    @Nonnull
    Collection<EventListener> findListeners(@Nonnull Object event);
}
