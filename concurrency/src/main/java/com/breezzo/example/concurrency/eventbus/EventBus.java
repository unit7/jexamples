package com.breezzo.example.concurrency.eventbus;

/**
 * Created by breezzo on 18.05.16.
 */
public interface EventBus {

    void post(Object event);

    void registerListener(Object listener);
}
