package com.breezzo.example.concurrency.eventbus.simple;

import com.breezzo.example.concurrency.eventbus.EventBus;
import com.breezzo.example.concurrency.eventbus.EventListener;
import com.breezzo.example.concurrency.eventbus.ListenerRegistryDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by breezzo on 18.05.16.
 */
public class SimpleEventBus implements EventBus {

    private static final Logger logger = LoggerFactory.getLogger(SimpleEventBus.class);

    private ListenerRegistryDelegate listenerRegistry = ListenerRegistryDelegate.of(new ListenerRegistrySimple());

    @Override
    public void post(Object event) {
        logger.trace("receive event {}", event);
        for (EventListener listener : listenerRegistry.findListeners(event)) {
            listener.handle(event);
        }
    }

    @Override
    public void registerListener(Object listener) {
        logger.trace("try to register listener {}", listener);
        listenerRegistry.registerListener(listener);
    }
}
