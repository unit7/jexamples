package com.breezzo.example.concurrency.eventbus.async;

import com.breezzo.example.concurrency.eventbus.EventListener;
import com.breezzo.example.concurrency.eventbus.ListenerRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by breezzo on 21.05.16.
 */
class ListenerRegistryAsync implements ListenerRegistry {

    private static final Logger logger = LoggerFactory.getLogger(ListenerRegistryAsync.class);

    private Map<Class<?>, List<EventListener>> listenerRegistry = new ConcurrentHashMap<>();

    private List<EventListener> getOrCreateListeners(Class<?> c) {
        List<EventListener> eventListeners = listenerRegistry.get(c);
        if (eventListeners == null) {
            listenerRegistry.putIfAbsent(c, new CopyOnWriteArrayList<>());
            eventListeners = listenerRegistry.get(c);
        }

        return eventListeners;
    }

    @Override
    public void addListener(@Nonnull EventListener eventListener, @Nonnull Class<?> listenerParamType) {
        logger.debug("add listener {} for {}", eventListener, listenerParamType);
        getOrCreateListeners(listenerParamType).add(eventListener);
    }

    @Nonnull
    @Override
    public Collection<EventListener> findListeners(@Nonnull Object event) {
        return getOrCreateListeners(event.getClass());
    }
}
