package com.breezzo.example.concurrency.collections.stack;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Многопоточная реализация стека без блокировок
 *
 * Created by breezzo on 17.05.16.
 *
 * ThreadSafe
 */
public class LockFreeStack<T> implements Stack<T> {

    private AtomicReference<Node<T>> head = new AtomicReference<>();
    private AtomicInteger size = new AtomicInteger(0);

    @Override
    public void push(T e) {

        Node<T> old, p;

        do {
            old = head.get();
            p = new Node<>(e, old);
        } while (!head.compareAndSet(old, p));

        size.incrementAndGet();
    }

    @Override
    public T pop() {

        Node<T> old, p;

        do {
            old = head.get();
            if (old == null) {
                return null;
            }

            p = old.prev;
        } while (!head.compareAndSet(old, p));

        size.decrementAndGet();
        return old.value;
    }

    @Override
    public int size() {
        return size.get();
    }

    @Override
    public boolean isEmpty() {
        return size.get() == 0;
    }

    private static class Node<T> {
        T value;
        Node<T> prev;

        public Node(T value, Node<T> prev) {
            this.value = value;
            this.prev = prev;
        }
    }
}
