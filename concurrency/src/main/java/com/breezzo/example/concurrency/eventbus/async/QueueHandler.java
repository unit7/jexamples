package com.breezzo.example.concurrency.eventbus.async;

import com.breezzo.example.concurrency.eventbus.EventListener;
import com.breezzo.example.concurrency.eventbus.ListenerRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by breezzo on 21.05.16.
 */
public class QueueHandler {

    private static final Logger logger = LoggerFactory.getLogger(QueueHandler.class);

    private static final int THREAD_COUNT = 10;
    private static final int TERMINATION_TIMEOUT = 10;

    private final BlockingQueue<Object> eventQueue;
    private final ListenerRegistry listenerRegistry;

    private ExecutorService executorService;

    QueueHandler(BlockingQueue<Object> eventQueue, ListenerRegistry listenerRegistry) {
        this.eventQueue = eventQueue;
        this.listenerRegistry = listenerRegistry;
    }

    public void setExecutorService(ExecutorService executorService) {
        this.executorService = executorService;
    }

    void start() {
        if (executorService == null) {
            executorService = Executors.newFixedThreadPool(THREAD_COUNT);
        }

        for (int i = 0; i < THREAD_COUNT; ++i) {
            executorService.execute(new HandlerEventTask());
        }
    }

    void stop() {
        executorService.shutdown();
        try {
            executorService.awaitTermination(TERMINATION_TIMEOUT, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            executorService.shutdownNow();
        }
    }

    private class HandlerEventTask implements Runnable {
        @Override
        public void run() {
            logger.info("Event handler started");

            QueueHandler queueHandler = QueueHandler.this;
            BlockingQueue<Object> eventQueue = queueHandler.eventQueue;
            ListenerRegistry listenerRegistry = queueHandler.listenerRegistry;

            while (true) {
                Object event;

                try {
                    event = eventQueue.poll(10, TimeUnit.MILLISECONDS);
                } catch (InterruptedException e) {
                    continue;
                }

                if (event != null) {
                    Collection<EventListener> handlers = listenerRegistry.findListeners(event);
                    logger.debug("Found {} handlers", handlers.size());
                    for (EventListener h : handlers) {
                        h.handle(event);
                    }
                }

                if (Thread.currentThread().isInterrupted()) {
                    logger.info("Thread interrupted externally, breaking loop");
                    break;
                }
            }
        }
    }
}
