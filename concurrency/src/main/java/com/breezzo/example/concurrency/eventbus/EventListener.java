package com.breezzo.example.concurrency.eventbus;

/**
 * Created by breezzo on 21.05.16.
 */
public interface EventListener {

    void handle(Object event);
}
