package com.breezzo.example.concurrency;

/**
 * Created by breezzo on 16.05.16.
 */
public class DoubleCheckedWithoutVolatile {
    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(() -> {
            Holder.getInstance();
        });
        thread.start();

        Holder.getInstance();
        thread.join();
    }

    private static class Holder {
        private static Holder instance;

        /**
         * happens-before на мониторе гарантирует, что кэш процессора будет очищен при входе в synchronized блок,
         * поэтому двойная инициализация исключена
         */

        static Holder getInstance() {
            if (instance == null) {
                System.out.println("instance null. " + threadInfo());
                synchronized (Holder.class) {
                    System.out.println("in sync block. " + threadInfo());
                    if (instance == null) {
                        instance = new Holder();
                    }
                }
            }

            return instance;
        }

        private Holder() {
            System.out.println("constructor. " + threadInfo());
        }

        private static String threadInfo() {
            return "Thread " + Thread.currentThread().getName() + " g " + Thread.currentThread().getThreadGroup().getName();
        }
    }
}
