package com.breezzo.example.concurrency.collections.stack;

/**
 * Created by breezzo on 17.05.16.
 */
public interface Stack<T> {

    void push(T e);

    T pop();

    int size();

    boolean isEmpty();
}
