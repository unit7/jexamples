package com.breezzo.example.concurrency.eventbus;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by breezzo on 18.05.16.
 */
public class BusTest {

    private static final int EVENTS_COUNT = 1000;

    @Test
    public void test() throws InterruptedException {

        List<Integer> events = Collections.synchronizedList(new ArrayList<>(EVENTS_COUNT));

        class SimpleEventListener {
            @Listened
            void handleEvent(Integer incomingEvent) {
                try {
                    TimeUnit.MILLISECONDS.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                events.add(incomingEvent);
            }

        }

        ExecutorService executorService = Executors.newFixedThreadPool(10);
        EventBus bus = EventBusFactory.getInstance().createAsync(executorService);
        SimpleEventListener eventListener = new SimpleEventListener();

        bus.registerListener(eventListener);

        long start = System.nanoTime();
        for (int i = 0; i < EVENTS_COUNT; ++i) {
            bus.post(1);
        }

        executorService.shutdown();
        executorService.awaitTermination(600, TimeUnit.MILLISECONDS);

        long end = System.nanoTime();
        System.out.println("time " + (end - start) / 1000000 + " ms");

        Assert.assertEquals(EVENTS_COUNT, events.size());
    }
}
