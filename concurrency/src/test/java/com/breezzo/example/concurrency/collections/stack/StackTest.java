package com.breezzo.example.concurrency.collections.stack;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by breezzo on 18.05.16.
 */
public class StackTest {

    @Test
    public void testBase() {
        Stack<Integer> stack = new LockFreeStack<>();
        stack.push(1);

        Assert.assertEquals(1, (long) stack.pop());

        stack.push(2);
        stack.push(3);

        Assert.assertEquals(3, (long) stack.pop());
        Assert.assertEquals(2, (long) stack.pop());

        stack.push(1);
        stack.push(2);
        stack.push(3);

        Assert.assertEquals(3, (long) stack.pop());
        Assert.assertEquals(2, (long) stack.pop());
        Assert.assertEquals(1, (long) stack.pop());

        Assert.assertEquals(null, stack.pop());
    }

    @Test
    public void testEmptySize() {
        Stack<Integer> stack = new LockFreeStack<>();

        Assert.assertTrue(stack.isEmpty());

        stack.push(1);

        Assert.assertEquals(1, stack.size());
        Assert.assertFalse(stack.isEmpty());

        stack.push(2);
        stack.push(3);

        Assert.assertEquals(3, stack.size());
        Assert.assertFalse(stack.isEmpty());

        stack.pop();
        stack.pop();

        Assert.assertEquals(1, stack.size());
        Assert.assertFalse(stack.isEmpty());

        stack.push(1);
        stack.push(2);
        stack.push(3);

        Assert.assertEquals(4, stack.size());
        Assert.assertFalse(stack.isEmpty());

        stack.pop();

        Assert.assertEquals(3, stack.size());
        Assert.assertFalse(stack.isEmpty());

        stack.pop();
        stack.pop();
        stack.pop();
        stack.pop();

        Assert.assertEquals(0, stack.size());
        Assert.assertTrue(stack.isEmpty());
    }
}
