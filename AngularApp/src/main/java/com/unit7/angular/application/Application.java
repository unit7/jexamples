/**
 * Date:	03 янв. 2015 г.
 * File:	Application.java
 *
 * Author:	Zajcev V.
 */

package com.unit7.angular.application;

import java.util.List;

import com.unit7.angular.db.DatabaseSessionFactory;
import com.unit7.angular.db.dao.ProductDAO;
import com.unit7.angular.model.Product;

/**
 * @author unit7
 *
 */
public class Application {
    private Application() {
        ProductDAO dao = DatabaseSessionFactory.instance().openSession().getMapper(ProductDAO.class);
        storedData = dao.getList();
    }

    public static Application instance() {
        return instance;
    }

    private static final Application instance = new Application();

    public List<Product> getStoredData() {
        return storedData;
    }

    public void setStoredData(List<Product> storedData) {
        this.storedData = storedData;
    }

    private List<Product> storedData;
}
