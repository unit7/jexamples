/**
 * Date:	03 янв. 2015 г.
 * File:	ProductRestService.java
 *
 * Author:	Zajcev V.
 */

package com.unit7.angular.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.unit7.angular.application.Application;
import com.unit7.angular.model.Product;

/**
 * @author unit7
 *
 */
@Path("product")
public class ProductRestService extends RestApplication {
    @GET
    @Path("list/get")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProducts() {
        GenericEntity<List<Product>> entity = new GenericEntity<List<Product>>(Application.instance().getStoredData(), List.class);
        return Response.ok(entity).build();
    }
}
