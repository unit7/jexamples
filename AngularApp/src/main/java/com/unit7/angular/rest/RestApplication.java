/**
 * Date:	03 янв. 2015 г.
 * File:	RestApplication.java
 *
 * Author:	Zajcev V.
 */

package com.unit7.angular.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * @author unit7
 *
 */
@ApplicationPath("rest")
public class RestApplication extends Application {
}
