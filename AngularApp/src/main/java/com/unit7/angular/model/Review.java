/**
 * Date:	03 янв. 2015 г.
 * File:	Review.java
 *
 * Author:	Zajcev V.
 */

package com.unit7.angular.model;

/**
 * @author unit7
 *
 */
public class Review {
    public Review() {
    }

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public long getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(long createdOn) {
        this.createdOn = createdOn;
    }

    private int stars;
    private String body;
    private String author;
    private long createdOn;
}
