/**
 * Date:	03 янв. 2015 г.
 * File:	Product.java
 *
 * Author:	Zajcev V.
 */

package com.unit7.angular.model;

import java.util.List;

/**
 * @author unit7
 *
 */
public class Product {
    public Product() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    private String name;
    private String description;
    private String spec;
    private String type;
    private double price;
    private List<Review> reviews;
}
