/**
 * Date:	04 янв. 2015 г.
 * File:	ProductDAO.java
 *
 * Author:	Zajcev V.
 */

package com.unit7.angular.db.dao;

import java.util.List;

import com.unit7.angular.model.Product;

/**
 * @author unit7
 *
 */
public interface ProductDAO {
    List<Product> getList();
}
