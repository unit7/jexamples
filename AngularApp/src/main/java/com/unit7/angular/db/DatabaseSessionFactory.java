/**
 * Date:	03 янв. 2015 г.
 * File:	DatabaseSessionFactory.java
 *
 * Author:	Zajcev V.
 */

package com.unit7.angular.db;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

/**
 * @author unit7
 *
 */
public class DatabaseSessionFactory {
    private static volatile DatabaseSessionFactory instance;
    
    private SqlSessionFactory sessionFactory;
    
    private DatabaseSessionFactory() {
        sessionFactory = new SqlSessionFactoryBuilder().build(DatabaseSessionFactory.class.getResourceAsStream("/mybatis-config.xml"));
    }
    
    public SqlSession openSession () {
        return sessionFactory.openSession();
    }

    public static DatabaseSessionFactory instance() {
        if (instance == null) {
            synchronized (DatabaseSessionFactory.class) {
                if (instance == null)
                    instance = new DatabaseSessionFactory();
            }
        }
        
        return instance;
    }
    
}
