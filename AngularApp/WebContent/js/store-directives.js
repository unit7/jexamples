(function() {
	var app = angular.module('store-directives', []);
	
	app.directive('productTabs', function() {
		return {
			restrict: 'E',
			templateUrl: 'content/product-tabs.html',
			controller: function() {
				this.tab = 1;
				this.setTab = function(value) {
					this.tab = value;
				};
				this.isSelected = function(value) {
					return this.tab === value;
				};
			},
			controllerAs: 'logic'
		};
	});
	
	app.directive('productTabsContent', function() {
		return {
			restrict: 'E',
			templateUrl: 'content/product-tabs-content.html'
		};
	});
	
	app.directive('reviewsTab', function() {
		return {
			restrict: 'E',
			templateUrl: 'content/reviews-tab.html'
		};
	});
})();