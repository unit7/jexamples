(function() {
	var root = localStorage.getItem('rootPath');
	var app = angular.module('StoreApp', ['store-directives']);
	app.controller('StoreController', ['$http', function($http) {
		this.products = [];
		this.appDescription = 'We have simple application that demonstrate us how we can manipulate data with Angular';
		
		var store = this;
		$http.get(root + 'rest/product/list/get').
			success(function(data) {
				store.products = data;
			}).
			error(function(data) {
				console.log(data);
			});
	}]);
	
	app.controller('ReviewController', function() {
		this.review = {};
		this.addReview = function(product) {
			this.review.createdOn = Date.now();
			product.reviews = product.reviews || [];
			product.reviews.push(this.review);
			this.review = {};
		};
	});
})();