<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html ng-app="StoreApp">
	<head>
		<link rel="stylesheet" href="<c:url value='/css/bootstrap.min.css' />"/>
		<link rel="stylesheet" href="<c:url value='/css/app_style.css' />" />
		
		<script type="text/javascript">
			localStorage.setItem("rootPath", "<c:url value='/' />");
		</script>
		
		<script type="text/javascript" src="<c:url value='/js/angular.min.js' />"></script>
		<script type="text/javascript" src="<c:url value='/js/app.js' />"></script>
		<script type="text/javascript" src="<c:url value='/js/store-directives.js' />"></script>
	</head>
	<body ng-controller="StoreController as store">
		<div><h3>{{store.appDescription}}</h3></div>
		<div style="margin-left: 10em;" ng-repeat="product in store.products">
			<h3>{{product.name}}</h3>
			<em class="pull-right">{{product.price | currency}}</em>
			<product-tabs></product-tabs>
		</div>
	</body>
</html>