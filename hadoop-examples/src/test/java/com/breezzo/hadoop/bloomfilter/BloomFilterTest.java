package com.breezzo.hadoop.bloomfilter;

import org.apache.hadoop.io.Writable;
import org.junit.Assert;
import org.junit.Test;

import java.io.*;

/**
 * Created by breezzo on 25.12.16.
 */
public class BloomFilterTest {

    @Test
    public void testAdd() {
        TestObject obj = new TestObject(10, 2.5, "123");
        BloomFilter<TestObject> filter = new BloomFilter<>();
        filter.add(obj);
    }

    @Test
    public void testContains() {
        BloomFilter<TestObject> filter = new BloomFilter<>();

        for (int i = 0; i < 10; ++i) {
            TestObject obj = new TestObject(10 + i, 2.5 + i, "123" + i);
            Assert.assertFalse(filter.contains(obj));

            filter.add(obj);

            Assert.assertTrue(filter.contains(obj));
        }
    }

    @Test
    public void testSerialization() throws IOException {
        BloomFilter<TestObject> filter = new BloomFilter<>();

        for (int i = 0; i < 10; ++i) {
            TestObject obj = new TestObject(10 + i, 2.5 + i, "123" + i);
            filter.add(obj);
        }

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        DataOutputStream dataOut = new DataOutputStream(out);
        filter.write(dataOut);

        byte[] serialized = out.toByteArray();
        DataInputStream dataIn = new DataInputStream(new ByteArrayInputStream(serialized));

        BloomFilter<TestObject> filter2 = new BloomFilter<>();
        filter2.readFields(dataIn);

        Assert.assertEquals(filter, filter2);
    }

    @Test
    public void testUnion() {
        BloomFilter<TestObject> first = new BloomFilter<>();
        BloomFilter<TestObject> second = new BloomFilter<>();

        TestObject f = new TestObject(1, 22, "asd");
        TestObject s = new TestObject(22, 2, "aaq");

        first.add(f);
        second.add(s);

        first.union(second);

        Assert.assertTrue(first.contains(f));
        Assert.assertTrue(first.contains(s));
    }

    private static class TestObject implements Writable {

        private int intNumber;
        private double doubleNumber;
        private String line;

        public TestObject(int intNumber, double doubleNumber, String line) {
            this.intNumber = intNumber;
            this.doubleNumber = doubleNumber;
            this.line = line;
        }

        @Override
        public void write(DataOutput out) throws IOException {
            out.writeInt(intNumber);
            out.writeDouble(doubleNumber);
            out.write(line.getBytes());
        }

        @Override
        public void readFields(DataInput in) throws IOException {
            intNumber = in.readInt();
            doubleNumber = in.readDouble();
            line = in.readUTF();
        }
    }
}
