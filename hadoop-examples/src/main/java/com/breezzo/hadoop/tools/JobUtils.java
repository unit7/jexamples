package com.breezzo.hadoop.tools;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.mapreduce.*;

import javax.annotation.Nullable;
import java.io.IOException;

/**
 * Created by breezzo on 17.12.16.
 */
public class JobUtils {

    public static Configuration defaultConfig() {
        Configuration conf = new Configuration();
        conf.addResource(new Path("/etc/hadoop/conf/core-site.xml"));
        return conf;
    }

    public static Job initJob(
            Configuration conf,
            Class<?> jobClass,
            Class<? extends Mapper> mapperClass,
            @Nullable Class<? extends Reducer> combinerClass,
            @Nullable Class<? extends Reducer> reducerClass,
            Class<? extends WritableComparable> outputKeyClass,
            Class<? extends Writable> outputValueClass,
            @Nullable Class<? extends InputFormat> inputFormatClass,
            @Nullable Class<? extends OutputFormat> outputFormatClass
    ) throws IOException {
        Job job = Job.getInstance(conf, jobClass.getName());

        if (combinerClass != null) {
            job.setCombinerClass(combinerClass);
        }
        if (reducerClass != null) {
            job.setReducerClass(reducerClass);
        } else {
            job.setNumReduceTasks(0);
        }

        job.setMapperClass(mapperClass);

        job.setJarByClass(jobClass);

        job.setOutputKeyClass(outputKeyClass);
        job.setOutputValueClass(outputValueClass);

        if (inputFormatClass != null) {
            job.setInputFormatClass(inputFormatClass);
        }

        if (outputFormatClass != null) {
            job.setOutputFormatClass(outputFormatClass);
        }

        return job;
    }

    public static Job initJob(
            Configuration conf,
            Class<?> jobClass,
            Class<? extends Mapper> mapperClass,
            @Nullable Class<? extends Reducer> reducerClass,
            Class<? extends WritableComparable> outputKeyClass,
            Class<? extends Writable> outputValueClass
    ) throws IOException {
        return initJob(
                conf,
                jobClass,
                mapperClass,
                null,
                reducerClass,
                outputKeyClass,
                outputValueClass,
                null,
                null
        );
    }
}
