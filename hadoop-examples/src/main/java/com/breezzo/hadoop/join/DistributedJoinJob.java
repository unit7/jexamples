package com.breezzo.hadoop.join;

import com.breezzo.hadoop.tools.JobUtils;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by breezzo on 18.12.16.
 */
public class DistributedJoinJob extends Configured implements Tool {

    public static void main(String[] args) throws Exception {
        System.exit(
                ToolRunner.run(
                        JobUtils.defaultConfig(),
                        new DistributedJoinJob(),
                        new String[] {
                                "/breezzo/join/input/customers",
                                "/breezzo/join/input/orders",
                                "/breezzo/join/output/c_o_joined"
                        }
                )
        );
    }

    @Override
    public int run(String[] args) throws Exception {

        Job job = JobUtils.initJob(
                getConf(),
                DistributedJoinJob.class,
                JoinMapper.class,
                null,
                NullWritable.class,
                Text.class
        );

        job.setCacheFiles(new URI[]{new Path(args[0]).toUri()});

        FileInputFormat.addInputPaths(job, args[1]);
        FileOutputFormat.setOutputPath(job, new Path(args[2]));

        return job.waitForCompletion(true) ? 0 : -1;
    }

    private static class JoinMapper extends Mapper<LongWritable, Text, NullWritable, Text> {

        private Map<String, String> cache = new HashMap<>();

        @Override
        protected void setup(Context context) throws IOException, InterruptedException {
            FileSystem fileSystem = FileSystem.get(context.getConfiguration());
            URI[] cacheFileUris = context.getCacheFiles();
            for (URI uri : cacheFileUris) {
                Path path = new Path(uri);
                Map<String, String> keyValues = readCache(path, fileSystem);
                cache.putAll(keyValues);
            }
        }

        private Map<String, String> readCache(Path path, FileSystem fileSystem) {
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(fileSystem.open(path)))) {
                return reader.lines().collect(Collectors.toMap(this::extractKey, Function.identity()));
            } catch (IOException e) {
                e.printStackTrace();
                throw new RuntimeException(e.getMessage());
            }
        }

        private String extractKey(String line) {
            int commaIndex = line.indexOf(',');
            return line.substring(0, commaIndex);
        }

        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            String order = value.toString();
            int commaIndex = order.indexOf(',');
            String customerId = order.substring(0, commaIndex);
            String customer = cache.get(customerId);

            if (customer != null) {
                context.write(NullWritable.get(), new Text(customer + "," + order));
            }
        }
    }
}
