package com.breezzo.hadoop.patent;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;

/**
 * Created by breezzo on 10.12.16.
 */
public class PatentLinksJob extends Configured implements Tool {

    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        conf.addResource(new Path("/home/breezzo/hadoop-2.6.4/etc/hadoop/core-site.xml"));

        conf.set("mapreduce.input.keyvaluelinerecordreader.key.value.separator", ",");

        ToolRunner.run(conf, new PatentLinksJob(), args);
    }

    @Override
    public int run(String[] args) throws Exception {
        Configuration conf = getConf();

        Job job = Job.getInstance(conf, "PatentLinks");

        job.setInputFormatClass(KeyValueTextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        job.setJarByClass(PatentLinksJob.class);

        job.setMapperClass(JobMapper.class);
        job.setReducerClass(JobReducer.class);

        FileInputFormat.setInputPaths(job, "/patents/input/cite75_99.txt");
        FileOutputFormat.setOutputPath(job, new Path("/patents/output/compacted"));

        boolean completed = job.waitForCompletion(true);
        System.out.println(completed);

        return completed ? 0 : -1;
    }

    private static final class JobMapper extends Mapper<Text, Text, Text, Text> {
        @Override
        protected void map(Text key, Text value, Context context) throws IOException, InterruptedException {
            context.write(key, value);
        }
    }

    private static final class JobReducer extends Reducer<Text, Text, Text, Text> {
        @Override
        protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
            StringBuilder csv = new StringBuilder();
            for (Text value : values) {
                if (csv.length() > 0) {
                    csv.append(",");
                }
                csv.append(value.toString());
            }

            context.write(key, new Text(csv.toString()));
        }
    }
}
