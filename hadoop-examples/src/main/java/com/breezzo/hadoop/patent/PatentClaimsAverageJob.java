package com.breezzo.hadoop.patent;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;

/**
 * Created by breezzo on 17.12.16.
 */
public class PatentClaimsAverageJob extends Configured implements Tool {
    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        conf.addResource(new Path("/home/breezzo/hadoop-2.6.4/etc/hadoop/core-site.xml"));

        ToolRunner.run(conf,
                new PatentClaimsAverageJob(),
                new String[] { "/patents/input/apat63_99.txt", "/patents/output/claims_average" });
    }

    @Override
    public int run(String[] args) throws Exception {
        Configuration conf = getConf();
        Job job = Job.getInstance(conf, "PatentClaimsAverageJob");

        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        FileInputFormat.setInputPaths(job, args[0]);
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        job.setMapperClass(JobMapper.class);
        job.setCombinerClass(JobCombiner.class);
        job.setReducerClass(JobReducer.class);

        boolean completed = job.waitForCompletion(true);
        System.out.println(completed);

        return completed ? 0 : -1;
    }

    private static class JobMapper extends Mapper<LongWritable, Text, Text, Text> {
        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            String[] fields = value.toString().split(",");
            String country = fields[4];
            String numClaims = fields[8];

            if (numClaims.length() > 0 && !numClaims.startsWith("\"")) {
                context.write(new Text(country.replaceAll("\"", "")), new Text(numClaims + ",1"));
            }
        }
    }

    private static class JobCombiner extends Reducer<Text, Text, Text, Text> {
        @Override
        protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
            long sum = 0;
            long count = 0;
            for (Text value : values) {
                String[] concat = value.toString().split(",");
                sum += Long.parseLong(concat[0]);
                count += Long.parseLong(concat[1]);
            }

            context.write(key, new Text(sum + "," + count));
        }
    }

    private static class JobReducer extends Reducer<Text, Text, Text, DoubleWritable> {
        @Override
        protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
            long sum = 0;
            long count = 0;
            for (Text value : values) {
                String[] concat = value.toString().split(",");
                sum += Long.parseLong(concat[0]);
                count += Long.parseLong(concat[1]);
            }

            double avg = (double) sum / count;

            context.write(key, new DoubleWritable(avg));
        }
    }
}
