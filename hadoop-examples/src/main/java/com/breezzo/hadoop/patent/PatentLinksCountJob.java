package com.breezzo.hadoop.patent;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;

/**
 * Created by breezzo on 10.12.16.
 */
public class PatentLinksCountJob extends Configured implements Tool {

    public static void main(String[] args) throws Exception {
        String[] toolArgs = new String[] { "/patents/input", "/patents/output/links_count" };
        Configuration conf = new Configuration();
        conf.addResource(new Path("/home/breezzo/hadoop-2.6.4/etc/hadoop/core-site.xml"));
        conf.set("mapreduce.input.keyvaluelinerecordreader.key.value.separator", ",");

        ToolRunner.run(conf, new PatentLinksCountJob(), toolArgs);
    }

    @Override
    public int run(String[] args) throws Exception {
        Configuration conf = getConf();
        Job job = Job.getInstance(conf, "PatentLinksCountJob");

        job.setInputFormatClass(KeyValueTextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        job.setMapperClass(JobMapper.class);
        job.setReducerClass(JobReducer.class);

        FileInputFormat.setInputPaths(job, args[0]);
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        boolean completed = job.waitForCompletion(true);
        System.out.println(completed);

        return completed ? 0 : -1;
    }

    private static class JobMapper extends Mapper<Text, Text, Text, Text> {
        @Override
        public void map(Text key, Text value, Context context) throws IOException, InterruptedException {
            context.write(key, value);
        }
    }

    private static class JobReducer extends Reducer<Text, Text, Text, IntWritable> {
        @Override
        public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
            int count = 0;
            for (Text value : values) {
                count += 1;
            }

            context.write(key, new IntWritable(count));
        }
    }
}
