package com.breezzo.hadoop.patent;

import com.breezzo.hadoop.tools.JobUtils;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;

/**
 * Created by breezzo on 10.12.16.
 */
public class PatentCountryGroupJob extends Configured implements Tool {

    private static final int COUNTER = 0;
    private static final String BASE_OUTPUT_PATH = "/patents/country/out" + COUNTER;

    private enum OUTPUT_NAME {
        OUT
    }

    public static void main(String[] args) throws Exception {
        System.exit(
                ToolRunner.run(JobUtils.defaultConfig(), new PatentCountryGroupJob(), new String[]{
                        "/patents/input/apat63_99.txt",
                        BASE_OUTPUT_PATH
                })
        );
    }

    @Override
    public int run(String[] args) throws Exception {
        Job job = JobUtils.initJob(
                getConf(),
                PatentCountryGroupJob.class,
                Mapper.class,
                JobReducer.class,
                LongWritable.class,
                Text.class);

        job.setInputFormatClass(TextInputFormat.class);

        FileInputFormat.setInputPaths(job, args[0]);
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        MultipleOutputs.addNamedOutput(
                job,
                OUTPUT_NAME.OUT.name(),
                TextOutputFormat.class,
                NullWritable.class,
                Text.class
        );

        boolean completed = job.waitForCompletion(true);

        return completed ? 0 : -1;
    }

    private static class JobReducer extends Reducer<LongWritable, Text, NullWritable, Text> {
        private MultipleOutputs<NullWritable, Text> mos;

        @Override
        protected void setup(Reducer.Context context) throws IOException, InterruptedException {
            mos = new MultipleOutputs<>(context);
        }

        @Override
        protected void reduce(LongWritable key,
                              Iterable<Text> values,
                              Context context) throws IOException, InterruptedException {
            for (Text value : values) {
                String country = value.toString().split(",")[4].replace("\"", "");
                String path = BASE_OUTPUT_PATH + "/" + country.toUpperCase();
                mos.write(OUTPUT_NAME.OUT.name(), NullWritable.get(), value, path);
            }
        }

        @Override
        protected void cleanup(Reducer.Context context) throws IOException, InterruptedException {
            mos.close();
        }
    }
}
