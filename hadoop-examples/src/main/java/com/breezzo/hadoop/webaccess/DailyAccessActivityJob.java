package com.breezzo.hadoop.webaccess;

import com.breezzo.hadoop.tools.JobUtils;
import org.apache.commons.math3.util.Pair;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;

/**
 * Created by breezzo on 17.12.16.
 */
public class DailyAccessActivityJob extends Configured implements Tool {

    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        conf.addResource(new Path("/home/breezzo/hadoop-2.6.4/etc/hadoop/core-site.xml"));

        System.exit(
                ToolRunner.run(conf, new DailyAccessActivityJob(), new String[]{
                        "/web/access/input/http-access.txt",
                        "/web/access/output/daily_activity"
                }));
    }

    @Override
    public int run(String[] args) throws Exception {
        Configuration conf = getConf();
        Job job = JobUtils.initJob(
                conf,
                DailyAccessActivityJob.class,
                JobMapper.class,
                JobReducer.class,
                JobReducer.class,
                IntWritable.class,
                IntWritable.class,
                TextInputFormat.class,
                TextOutputFormat.class
        );

        job.setJarByClass(DailyAccessActivityJob.class);

        Path input = new Path(args[0]);
        Path output = new Path(args[1]);
        FileInputFormat.addInputPath(job, input);
        FileOutputFormat.setOutputPath(job, output);

        boolean completed = job.waitForCompletion(true);
        System.out.println(completed);

        return completed ? 0 : -1;
    }

    private static class JobMapper extends Mapper<LongWritable, Text, IntWritable, IntWritable> {
        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            String accessLine = value.toString();
            Pair<Integer, Integer> dayAndHour = toDayAndHour(accessLine);
            if (dayAndHour.getFirst() == 30) {
                Integer hour = dayAndHour.getSecond();
                context.write(new IntWritable(hour), new IntWritable(1));
            }
        }

        private Pair<Integer, Integer> toDayAndHour(String line) {
            String[] parts = line.split(" ");
            String datePart = parts[1];
            datePart = datePart.substring(1, datePart.length() - 1);
            String[] dateParts = datePart.split(":");
            String day = dateParts[0];
            String hour = dateParts[1];

            return new Pair<>(Integer.parseInt(day), Integer.parseInt(hour));
        }
    }

    private static class JobReducer extends Reducer<IntWritable, IntWritable, IntWritable, IntWritable> {
        @Override
        protected void reduce(IntWritable key, Iterable<IntWritable> values,
                              Context context) throws IOException, InterruptedException {
            int result = 0;
            for (IntWritable value : values) {
                result += value.get();
            }

            context.write(key, new IntWritable(result));
        }
    }
}
