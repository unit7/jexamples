package com.breezzo.hadoop.proto;

import com.breezzo.hadoop.tools.JobUtils;
import com.breezzo.proto.ProtoTest;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.ipc.protobuf.ProtobufRpcEngineProtos;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;

/**
 * Created by breezzo on 28.12.16.
 */
public class ProtoTestJob extends Configured implements Tool {

    private static final int COUNTER = 9;

    public static void main(String[] args) throws Exception {
        System.exit(
                ToolRunner.run(JobUtils.defaultConfig(), new ProtoTestJob(), new String[]{
                        "/proto_test/proto_test.pb",
                        "/proto_test/out" + COUNTER
                })
        );
    }

    @Override
    public int run(String[] args) throws Exception {
        Job job = JobUtils.initJob(
                getConf(),
                ProtoTestJob.class,
                Map.class,
                null,
                null,
                NullWritable.class,
                Text.class,
                TextInputFormat.class,
                TextOutputFormat.class
        );

        FileInputFormat.addInputPaths(job, args[0]);
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        return job.waitForCompletion(true) ? 0 : -1;
    }

    private static class Map extends Mapper<LongWritable, Text, NullWritable, Text> {
        @Override
        protected void setup(Context context) throws IOException, InterruptedException {
            ProtobufRpcEngineProtos.getDescriptor();
        }

        @Override
        protected void map(LongWritable key,
                           Text value,
                           Context context) throws IOException, InterruptedException {

            try {
                ProtoTest.TestModelNewProto test = ProtoTest.TestModelNewProto.parseFrom(value.getBytes());

                String containingJar = getJarString(test.getTestsList().getClass());
                String containingJar2 = getJarString(ProtobufRpcEngineProtos.getDescriptor().getClass());

                String result = String.format("jar: %s, testId: %d, name: %s, tests: %s",
                        containingJar + ";" + containingJar2,
                        test.getTestId(),
                        test.getTestName(),
                        test.getTestsList().toString());

                context.write(NullWritable.get(), new Text(result));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private String getJarString(Class<?> aClass) throws IOException {
            StringBuilder rs = new StringBuilder();
            String classFile = aClass.getName().replaceAll("\\.", "/") + ".class";
            ClassLoader classLoader = aClass.getClassLoader();
            Enumeration<URL> resources = classLoader.getResources(classFile);
            while (resources.hasMoreElements()) {
                URL url = resources.nextElement();
                rs.append(url.getProtocol());
                rs.append("*");
                rs.append(url.getPath());
                rs.append(";");
            }
            return rs.toString();
        }
    }
}
