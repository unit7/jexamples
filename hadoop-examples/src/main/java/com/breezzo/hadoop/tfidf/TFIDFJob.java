package com.breezzo.hadoop.tfidf;

import com.breezzo.hadoop.tools.JobUtils;
import org.apache.commons.math3.util.Pair;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by breezzo on 17.12.16.
 */
public class TFIDFJob extends Configured implements Tool {

    private static final int DOC_COUNT = 4;
    private static final int JOB_NUM = 0;

    public static void main(String[] args) throws Exception {
        System.exit(
                ToolRunner.run(JobUtils.defaultConfig(), new TFIDFJob(), new String[] {
                        "/breezzo/text/tfidf/input/doc",
                        "/breezzo/text/tfidf/input/doc1",
                        "/breezzo/text/tfidf/input/doc2",
                        "/breezzo/text/tfidf/input/doc3",
                        "/breezzo/text/tfidf/output/term_count" + JOB_NUM
                })
        );
    }

    @Override
    public int run(String[] args) throws Exception {

        // посчитаем количество уникальных термов в каждом документе
        Job uniqTermJob = JobUtils.initJob(
                getConf(),
                TFIDFJob.class,
                UniqTermMapper.class,
                UniqTermReducer.class,
                Text.class,
                IntWritable.class
        );

        StringBuilder inputPaths = new StringBuilder();
        for (int i = 0; i < args.length - 1; ++i) {
            inputPaths.append(args[i]);
            if (i < args.length - 2) {
                inputPaths.append(",");
            }
        }

        FileInputFormat.addInputPaths(uniqTermJob, inputPaths.toString());
        FileOutputFormat.setOutputPath(uniqTermJob, new Path(args[args.length - 1]));

        boolean uniqTermCompleted = uniqTermJob.waitForCompletion(true);
        if (!uniqTermCompleted) {
            return -1;
        }

        // посчитаем общее количество термов для каждого документа
        Job docTermCountJob = JobUtils.initJob(
                getConf(),
                TFIDFJob.class,
                DocTermCountMapper.class,
                DocTermCountReducer.class,
                Text.class,
                Text.class
        );

        int jobCount = JOB_NUM;
        String docTermCountPath = "/breezzo/text/tfidf/output/doc_term_count" + jobCount;

        FileInputFormat.addInputPaths(docTermCountJob, args[args.length - 1] + "/part-r-00000");
        FileOutputFormat.setOutputPath(docTermCountJob, new Path(docTermCountPath));

        boolean docTermCountCompleted = docTermCountJob.waitForCompletion(true);
        if (!docTermCountCompleted) {
            return -1;
        }

        // посчитаем количество документов для каждого терма, в которых данный терм содержится
        Job termDocCountJob = JobUtils.initJob(
                getConf(),
                TFIDFJob.class,
                TermDocCountMapper.class,
                TermDocCountReducer.class,
                Text.class,
                Text.class
        );

        String termDocCountPath = "/breezzo/text/tfidf/output/term_doc_count" + jobCount;

        FileInputFormat.addInputPaths(termDocCountJob, docTermCountPath + "/part-r-00000");
        FileOutputFormat.setOutputPath(termDocCountJob, new Path(termDocCountPath));

        boolean termDocCountCompleted = termDocCountJob.waitForCompletion(true);

        if (!termDocCountCompleted) {
            return -1;
        }

        // расчёт характеристики tf-idf для каждого терма по каждому документу
        // tf = n / t, n - количество термов в документе, t - количество вхождений указанного терма в документ
        // idf = log( |D| / |n| ), D - количество документов в корпусе, n - количство документов, в которых содержится терм
        Job tfIdfJob = JobUtils.initJob(
                getConf(),
                TFIDFJob.class,
                TFIDFMapper.class,
                Reducer.class,
                Text.class,
                DoubleWritable.class
        );

        String tfIdfPath = "/breezzo/text/tfidf/output/tf_idf" + jobCount;

        FileInputFormat.addInputPaths(tfIdfJob, termDocCountPath + "/part-r-00000");
        FileOutputFormat.setOutputPath(tfIdfJob, new Path(tfIdfPath));

        boolean tfIdfCompleted = tfIdfJob.waitForCompletion(true);

        return tfIdfCompleted ? 0 : -1;

    }

    private static class UniqTermMapper extends Mapper<LongWritable, Text, Text, IntWritable> {

        private static final IntWritable ONE = new IntWritable(1);

        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            String[] terms = value.toString().split("\\s");
            for (String t : terms) {
                context.write(makeKey(t, context), ONE);
            }
        }

        private Text makeKey(String term, Context context) {
            String inputFileName = ((FileSplit) context.getInputSplit()).getPath().getName();
            return new Text(inputFileName + "_" + term);
        }
    }

    private static class UniqTermReducer extends Reducer<Text, IntWritable, NullWritable, Text> {
        @Override
        protected void reduce(Text key, Iterable<IntWritable> values,
                              Context context) throws IOException, InterruptedException {
            int count = 0;
            for (IntWritable v : values) {
                count += v.get();
            }

            context.write(NullWritable.get(), makeValue(key, count));
        }

        private Text makeValue(Text prefix, int suffix) {
            return new Text(prefix.toString() + "_" + suffix);
        }
    }

    private static class DocTermCountMapper extends Mapper<LongWritable, Text, Text, Text> {
        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            String composite = value.toString();
            Pair<String, String> splitedKey = splitKey(composite);

            context.write(new Text(splitedKey.getKey()), new Text(splitedKey.getValue()));
        }

        private Pair<String, String> splitKey(String key) {
            int index = key.indexOf("_");
            String docId = key.substring(0, index);
            String other = key.substring(index + 1);
            return new Pair<>(docId, other);
        }
    }

    private static class DocTermCountReducer extends Reducer<Text, Text, NullWritable, Text> {
        @Override
        protected void reduce(Text key, Iterable<Text> values,
                              Context context) throws IOException, InterruptedException {
            String docId = key.toString();
            int commonTermCount = 0;
            List<String> result = new ArrayList<>();
            for (Text v : values) {
                String value = v.toString();
                int termCount = getTermCount(value);
                commonTermCount += termCount;
                result.add(docId + "_" + value + "_");
            }

            for (String v : result) {
                context.write(NullWritable.get(), new Text(v + commonTermCount));
            }
        }

        private int getTermCount(String v) {
            int index = v.indexOf("_");
            String stringCount = v.substring(index + 1);
            return Integer.parseInt(stringCount);
        }
    }

    private static class TermDocCountMapper extends Mapper<LongWritable, Text, Text, Text> {
        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            Pair<String, String> splitedKey = splitKey(value.toString());
            context.write(new Text(splitedKey.getFirst()), new Text(splitedKey.getSecond()));
        }

        private Pair<String, String> splitKey(String key) {
            int termIndex = key.indexOf("_") + 1;
            int termEndIndex = key.indexOf("_", termIndex);

            String term = key.substring(termIndex, termEndIndex);
            String other = key.substring(0, termIndex) + key.substring(termEndIndex + 1);

            return new Pair<>(term, other);
        }
    }

    private static class TermDocCountReducer extends Reducer<Text, Text, NullWritable, Text> {
        @Override
        protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
            Set<String> docSet = new HashSet<>();
            List<String> result = new ArrayList<>();
            String term = key.toString();

            for (Text v : values) {
                String value = v.toString();
                Pair<String, String> splitedKey = splitKey(value);
                docSet.add(splitedKey.getFirst());

                result.add(splitedKey.getFirst() + "_" + term + splitedKey.getSecond() + "_");
            }

            for (String r : result) {
                context.write(NullWritable.get(), new Text(r + docSet.size()));
            }
        }

        private Pair<String, String> splitKey(String key) {
            int index = key.indexOf("_");
            String docId = key.substring(0, index);
            String other = key.substring(index);

            return new Pair<>(docId, other);
        }
    }

    private static class TFIDFMapper extends Mapper<LongWritable, Text, Text, DoubleWritable> {
        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            String data = value.toString();
            int index = data.indexOf("_");
            int statIndex = data.indexOf("_", index + 1) + 1;
            int commonTermCountIndex = data.indexOf("_", statIndex) + 1;
            int termDocCountIndex = data.indexOf("_", commonTermCountIndex) + 1;

            String docIdTerm = data.substring(0, statIndex - 1);

            int termCount = Integer.parseInt(data.substring(statIndex, commonTermCountIndex - 1));
            int commonTermCount = Integer.parseInt(data.substring(commonTermCountIndex, termDocCountIndex - 1));
            int termDocCount = Integer.parseInt(data.substring(termDocCountIndex));

            context.write(
                    new Text(docIdTerm),
                    new DoubleWritable(
                            calcValue(termCount, commonTermCount, termDocCount)
                    )
            );
        }

        private double calcValue(int termCount, int commonTermCount, int termDocCount) {
            double tf = (double) commonTermCount / termCount;
            double idf = Math.log((double) DOC_COUNT / termDocCount);

            return tf * idf;
        }
    }
}
