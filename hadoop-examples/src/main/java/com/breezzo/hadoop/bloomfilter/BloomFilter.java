package com.breezzo.hadoop.bloomfilter;

import com.google.common.hash.*;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Random;

/**
 * Created by breezzo on 24.12.16.
 */
public class BloomFilter<T extends Writable> implements Writable {

    private static final int DEFAULT_MAX_OBJECT_COUNT = 10000;
    private static final int DEFAULT_BITS_COUNT = 8 * DEFAULT_MAX_OBJECT_COUNT;

    private BitSet hashSet;
    private int numHashFunctions;

    public BloomFilter() {
        this(DEFAULT_BITS_COUNT, DEFAULT_MAX_OBJECT_COUNT);
    }

    public BloomFilter(int bitsCount, int maxObjectCount) {
        numHashFunctions = (int) Math.round(0.7 * (bitsCount / maxObjectCount));
        hashSet = new BitSet(bitsCount);
    }

    public void add(T obj) {
        int[] hashIndexArray = makeHashIndexArray(obj);
        for (int i = 0; i < hashIndexArray.length; i++) {
            hashSet.set(hashIndexArray[i]);
        }
    }

    public boolean contains(T obj) {
        int[] hashIndexArray = makeHashIndexArray(obj);
        for (int i = 0; i < hashIndexArray.length; i++) {
            if (!hashSet.get(hashIndexArray[i])) {
                return false;
            }
        }

        return true;
    }

    public void union(BloomFilter<T> other) {
        hashSet.or(other.hashSet);
    }

    private int[] makeHashIndexArray(T obj) {
        int[] hashIndexArray = new int[numHashFunctions];
        byte[] objectBytes = serialize(obj);

        HashFunction hasher = Hashing.murmur3_128();
        HashCode hashCode = hasher.hashObject(objectBytes, Funnels.byteArrayFunnel());

        long seed = hashCode.asLong();
        Random random = new Random(seed);

        for (int i = 0; i < numHashFunctions; i++) {
            hashIndexArray[i] = random.nextInt(hashSet.size());
        }

        return hashIndexArray;
    }

    private byte[] serialize(T obj) {
        try {
            ByteArrayDataOutput dataOutput = ByteStreams.newDataOutput();
            obj.write(dataOutput);
            return dataOutput.toByteArray();
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @Override
    public void write(DataOutput out) throws IOException {
        byte tmp = 0;
        int bitCounter = 0;
        int byteCounter = 0;
        byte[] result = new byte[hashSet.size() / 8 + 1];

        for (int i = 0; i < hashSet.size(); ++i) {
            if (bitCounter > 7) {
                bitCounter = 0;
                result[byteCounter++] = tmp;
                tmp = 0;
            }
            tmp = (byte) (tmp | (toInt(hashSet.get(i)) << bitCounter++));
        }

        if (result.length > byteCounter) {
            result = Arrays.copyOf(result, byteCounter);
        }

        out.writeInt(numHashFunctions);
        out.writeInt(hashSet.size());
        out.writeInt(byteCounter);
        out.write(result);
    }

    private int toInt(boolean b) {
        return b ? 1 : 0;
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        numHashFunctions = in.readInt();
        int hashSetSize = in.readInt();
        int hashSetBytesLength = in.readInt();
        byte[] hashSetBytes = new byte[hashSetBytesLength];
        in.readFully(hashSetBytes);

        hashSet = new BitSet(hashSetSize);
        for (int i = 0; i < hashSetBytesLength; ++i) {
            byte tmp = hashSetBytes[i];
            for (int j = 0; j < 8; ++j) {
                if ((tmp & (1 << j)) > 0) {
                    hashSet.set(i * 8 + j);
                }
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        BloomFilter<?> that = (BloomFilter<?>) o;

        return new org.apache.commons.lang3.builder.EqualsBuilder()
                .append(numHashFunctions, that.numHashFunctions)
                .append(hashSet, that.hashSet)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new org.apache.commons.lang3.builder.HashCodeBuilder(17, 37)
                .append(hashSet)
                .append(numHashFunctions)
                .toHashCode();
    }
}
