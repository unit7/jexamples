package com.breezzo.hadoop.bloomfilter;

import com.breezzo.hadoop.tools.JobUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.NullOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;

/**
 * Created by breezzo on 04.01.17.
 */
public class BloomFilterJob extends Configured implements Tool {

    private static final int COUNTER = 0;

    public static void main(String[] args) throws Exception {
        System.exit(
                ToolRunner.run(JobUtils.defaultConfig(), new BloomFilterJob(), new String[]{
                        "/bloom/input",
                        "/bloom/out" + COUNTER
                })
        );
    }

    @Override
    public int run(String[] args) throws Exception {
        Job job = JobUtils.initJob(
                getConf(),
                BloomFilterJob.class,
                Map.class,
                null,
                Reduce.class,
                Text.class,
                BloomFilter.class,
                TextInputFormat.class,
                NullOutputFormat.class
        );

        FileInputFormat.addInputPaths(job, args[0]);
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        return job.waitForCompletion(true) ? 0 : -1;
    }

    private static class Map extends Mapper<LongWritable, Text, Text, BloomFilter<Text>> {

        private BloomFilter<Text> filter = new BloomFilter<>();

        @Override
        protected void map(LongWritable key, Text value, Context context)
                throws IOException, InterruptedException {
            filter.add(value);
        }

        @Override
        protected void cleanup(Context context) throws IOException, InterruptedException {
            context.write(new Text("test"), filter);
        }
    }

    private static class Reduce extends Reducer<Text, BloomFilter<Text>, Text, Text> {
        @Override
        protected void reduce(Text key, Iterable<BloomFilter<Text>> values, Context context)
                throws IOException, InterruptedException {
            BloomFilter<Text> result = new BloomFilter<>();
            for (BloomFilter<Text> f : values) {
                result.union(f);
            }

            Configuration conf = context.getConfiguration();
            Path path = new Path(conf.get("mapred.output.dir") + "/bloomfilter");
            try (FSDataOutputStream fsOut = FileSystem.get(conf).create(path)) {
                result.write(fsOut);
            }
        }
    }
}
