// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ProtoTest.proto

package com.breezzo.proto;

public final class ProtoTest {
    private ProtoTest() {}
    public static void registerAllExtensions(
            com.google.protobuf.ExtensionRegistry registry) {
    }
    public interface TestModelNewProtoOrBuilder extends
            // @@protoc_insertion_point(interface_extends:Breezzo.Proto.TestModelNewProto)
            com.google.protobuf.MessageOrBuilder {

        /**
         * <code>optional int64 test_id = 1;</code>
         */
        boolean hasTestId();
        /**
         * <code>optional int64 test_id = 1;</code>
         */
        long getTestId();

        /**
         * <code>optional string test_name = 2;</code>
         */
        boolean hasTestName();
        /**
         * <code>optional string test_name = 2;</code>
         */
        java.lang.String getTestName();
        /**
         * <code>optional string test_name = 2;</code>
         */
        com.google.protobuf.ByteString
        getTestNameBytes();

        /**
         * <code>repeated string tests = 3;</code>
         */
        com.google.protobuf.ProtocolStringList
        getTestsList();
        /**
         * <code>repeated string tests = 3;</code>
         */
        int getTestsCount();
        /**
         * <code>repeated string tests = 3;</code>
         */
        java.lang.String getTests(int index);
        /**
         * <code>repeated string tests = 3;</code>
         */
        com.google.protobuf.ByteString
        getTestsBytes(int index);
    }
    /**
     * Protobuf type {@code Breezzo.Proto.TestModelNewProto}
     */
    public static final class TestModelNewProto extends
            com.google.protobuf.GeneratedMessage implements
            // @@protoc_insertion_point(message_implements:Breezzo.Proto.TestModelNewProto)
            TestModelNewProtoOrBuilder {
        // Use TestModelNewProto.newBuilder() to construct.
        private TestModelNewProto(com.google.protobuf.GeneratedMessage.Builder<?> builder) {
            super(builder);
            this.unknownFields = builder.getUnknownFields();
        }
        private TestModelNewProto(boolean noInit) { this.unknownFields = com.google.protobuf.UnknownFieldSet.getDefaultInstance(); }

        private static final TestModelNewProto defaultInstance;
        public static TestModelNewProto getDefaultInstance() {
            return defaultInstance;
        }

        public TestModelNewProto getDefaultInstanceForType() {
            return defaultInstance;
        }

        private final com.google.protobuf.UnknownFieldSet unknownFields;
        @java.lang.Override
        public final com.google.protobuf.UnknownFieldSet
        getUnknownFields() {
            return this.unknownFields;
        }
        private TestModelNewProto(
                com.google.protobuf.CodedInputStream input,
                com.google.protobuf.ExtensionRegistryLite extensionRegistry)
                throws com.google.protobuf.InvalidProtocolBufferException {
            initFields();
            int mutable_bitField0_ = 0;
            com.google.protobuf.UnknownFieldSet.Builder unknownFields =
                    com.google.protobuf.UnknownFieldSet.newBuilder();
            try {
                boolean done = false;
                while (!done) {
                    int tag = input.readTag();
                    switch (tag) {
                        case 0:
                            done = true;
                            break;
                        default: {
                            if (!parseUnknownField(input, unknownFields,
                                    extensionRegistry, tag)) {
                                done = true;
                            }
                            break;
                        }
                        case 8: {
                            bitField0_ |= 0x00000001;
                            testId_ = input.readInt64();
                            break;
                        }
                        case 18: {
                            com.google.protobuf.ByteString bs = input.readBytes();
                            bitField0_ |= 0x00000002;
                            testName_ = bs;
                            break;
                        }
                        case 26: {
                            com.google.protobuf.ByteString bs = input.readBytes();
                            if (!((mutable_bitField0_ & 0x00000004) == 0x00000004)) {
                                tests_ = new com.google.protobuf.LazyStringArrayList();
                                mutable_bitField0_ |= 0x00000004;
                            }
                            tests_.add(bs);
                            break;
                        }
                    }
                }
            } catch (com.google.protobuf.InvalidProtocolBufferException e) {
                throw e.setUnfinishedMessage(this);
            } catch (java.io.IOException e) {
                throw new com.google.protobuf.InvalidProtocolBufferException(
                        e.getMessage()).setUnfinishedMessage(this);
            } finally {
                if (((mutable_bitField0_ & 0x00000004) == 0x00000004)) {
                    tests_ = tests_.getUnmodifiableView();
                }
                this.unknownFields = unknownFields.build();
                makeExtensionsImmutable();
            }
        }
        public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
            return com.breezzo.proto.ProtoTest.internal_static_Breezzo_Proto_TestModelNewProto_descriptor;
        }

        protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
        internalGetFieldAccessorTable() {
            return com.breezzo.proto.ProtoTest.internal_static_Breezzo_Proto_TestModelNewProto_fieldAccessorTable
                    .ensureFieldAccessorsInitialized(
                            com.breezzo.proto.ProtoTest.TestModelNewProto.class, com.breezzo.proto.ProtoTest.TestModelNewProto.Builder.class);
        }

        public static com.google.protobuf.Parser<TestModelNewProto> PARSER =
                new com.google.protobuf.AbstractParser<TestModelNewProto>() {
                    public TestModelNewProto parsePartialFrom(
                            com.google.protobuf.CodedInputStream input,
                            com.google.protobuf.ExtensionRegistryLite extensionRegistry)
                            throws com.google.protobuf.InvalidProtocolBufferException {
                        return new TestModelNewProto(input, extensionRegistry);
                    }
                };

        @java.lang.Override
        public com.google.protobuf.Parser<TestModelNewProto> getParserForType() {
            return PARSER;
        }

        private int bitField0_;
        public static final int TEST_ID_FIELD_NUMBER = 1;
        private long testId_;
        /**
         * <code>optional int64 test_id = 1;</code>
         */
        public boolean hasTestId() {
            return ((bitField0_ & 0x00000001) == 0x00000001);
        }
        /**
         * <code>optional int64 test_id = 1;</code>
         */
        public long getTestId() {
            return testId_;
        }

        public static final int TEST_NAME_FIELD_NUMBER = 2;
        private java.lang.Object testName_;
        /**
         * <code>optional string test_name = 2;</code>
         */
        public boolean hasTestName() {
            return ((bitField0_ & 0x00000002) == 0x00000002);
        }
        /**
         * <code>optional string test_name = 2;</code>
         */
        public java.lang.String getTestName() {
            java.lang.Object ref = testName_;
            if (ref instanceof java.lang.String) {
                return (java.lang.String) ref;
            } else {
                com.google.protobuf.ByteString bs =
                        (com.google.protobuf.ByteString) ref;
                java.lang.String s = bs.toStringUtf8();
                if (bs.isValidUtf8()) {
                    testName_ = s;
                }
                return s;
            }
        }
        /**
         * <code>optional string test_name = 2;</code>
         */
        public com.google.protobuf.ByteString
        getTestNameBytes() {
            java.lang.Object ref = testName_;
            if (ref instanceof java.lang.String) {
                com.google.protobuf.ByteString b =
                        com.google.protobuf.ByteString.copyFromUtf8(
                                (java.lang.String) ref);
                testName_ = b;
                return b;
            } else {
                return (com.google.protobuf.ByteString) ref;
            }
        }

        public static final int TESTS_FIELD_NUMBER = 3;
        private com.google.protobuf.LazyStringList tests_;
        /**
         * <code>repeated string tests = 3;</code>
         */
        public com.google.protobuf.ProtocolStringList
        getTestsList() {
            return tests_;
        }
        /**
         * <code>repeated string tests = 3;</code>
         */
        public int getTestsCount() {
            return tests_.size();
        }
        /**
         * <code>repeated string tests = 3;</code>
         */
        public java.lang.String getTests(int index) {
            return tests_.get(index);
        }
        /**
         * <code>repeated string tests = 3;</code>
         */
        public com.google.protobuf.ByteString
        getTestsBytes(int index) {
            return tests_.getByteString(index);
        }

        private void initFields() {
            testId_ = 0L;
            testName_ = "";
            tests_ = com.google.protobuf.LazyStringArrayList.EMPTY;
        }
        private byte memoizedIsInitialized = -1;
        public final boolean isInitialized() {
            byte isInitialized = memoizedIsInitialized;
            if (isInitialized == 1) return true;
            if (isInitialized == 0) return false;

            memoizedIsInitialized = 1;
            return true;
        }

        public void writeTo(com.google.protobuf.CodedOutputStream output)
                throws java.io.IOException {
            getSerializedSize();
            if (((bitField0_ & 0x00000001) == 0x00000001)) {
                output.writeInt64(1, testId_);
            }
            if (((bitField0_ & 0x00000002) == 0x00000002)) {
                output.writeBytes(2, getTestNameBytes());
            }
            for (int i = 0; i < tests_.size(); i++) {
                output.writeBytes(3, tests_.getByteString(i));
            }
            getUnknownFields().writeTo(output);
        }

        private int memoizedSerializedSize = -1;
        public int getSerializedSize() {
            int size = memoizedSerializedSize;
            if (size != -1) return size;

            size = 0;
            if (((bitField0_ & 0x00000001) == 0x00000001)) {
                size += com.google.protobuf.CodedOutputStream
                        .computeInt64Size(1, testId_);
            }
            if (((bitField0_ & 0x00000002) == 0x00000002)) {
                size += com.google.protobuf.CodedOutputStream
                        .computeBytesSize(2, getTestNameBytes());
            }
            {
                int dataSize = 0;
                for (int i = 0; i < tests_.size(); i++) {
                    dataSize += com.google.protobuf.CodedOutputStream
                            .computeBytesSizeNoTag(tests_.getByteString(i));
                }
                size += dataSize;
                size += 1 * getTestsList().size();
            }
            size += getUnknownFields().getSerializedSize();
            memoizedSerializedSize = size;
            return size;
        }

        private static final long serialVersionUID = 0L;
        @java.lang.Override
        protected java.lang.Object writeReplace()
                throws java.io.ObjectStreamException {
            return super.writeReplace();
        }

        public static com.breezzo.proto.ProtoTest.TestModelNewProto parseFrom(
                com.google.protobuf.ByteString data)
                throws com.google.protobuf.InvalidProtocolBufferException {
            return PARSER.parseFrom(data);
        }
        public static com.breezzo.proto.ProtoTest.TestModelNewProto parseFrom(
                com.google.protobuf.ByteString data,
                com.google.protobuf.ExtensionRegistryLite extensionRegistry)
                throws com.google.protobuf.InvalidProtocolBufferException {
            return PARSER.parseFrom(data, extensionRegistry);
        }
        public static com.breezzo.proto.ProtoTest.TestModelNewProto parseFrom(byte[] data)
                throws com.google.protobuf.InvalidProtocolBufferException {
            return PARSER.parseFrom(data);
        }
        public static com.breezzo.proto.ProtoTest.TestModelNewProto parseFrom(
                byte[] data,
                com.google.protobuf.ExtensionRegistryLite extensionRegistry)
                throws com.google.protobuf.InvalidProtocolBufferException {
            return PARSER.parseFrom(data, extensionRegistry);
        }
        public static com.breezzo.proto.ProtoTest.TestModelNewProto parseFrom(java.io.InputStream input)
                throws java.io.IOException {
            return PARSER.parseFrom(input);
        }
        public static com.breezzo.proto.ProtoTest.TestModelNewProto parseFrom(
                java.io.InputStream input,
                com.google.protobuf.ExtensionRegistryLite extensionRegistry)
                throws java.io.IOException {
            return PARSER.parseFrom(input, extensionRegistry);
        }
        public static com.breezzo.proto.ProtoTest.TestModelNewProto parseDelimitedFrom(java.io.InputStream input)
                throws java.io.IOException {
            return PARSER.parseDelimitedFrom(input);
        }
        public static com.breezzo.proto.ProtoTest.TestModelNewProto parseDelimitedFrom(
                java.io.InputStream input,
                com.google.protobuf.ExtensionRegistryLite extensionRegistry)
                throws java.io.IOException {
            return PARSER.parseDelimitedFrom(input, extensionRegistry);
        }
        public static com.breezzo.proto.ProtoTest.TestModelNewProto parseFrom(
                com.google.protobuf.CodedInputStream input)
                throws java.io.IOException {
            return PARSER.parseFrom(input);
        }
        public static com.breezzo.proto.ProtoTest.TestModelNewProto parseFrom(
                com.google.protobuf.CodedInputStream input,
                com.google.protobuf.ExtensionRegistryLite extensionRegistry)
                throws java.io.IOException {
            return PARSER.parseFrom(input, extensionRegistry);
        }

        public static Builder newBuilder() { return Builder.create(); }
        public Builder newBuilderForType() { return newBuilder(); }
        public static Builder newBuilder(com.breezzo.proto.ProtoTest.TestModelNewProto prototype) {
            return newBuilder().mergeFrom(prototype);
        }
        public Builder toBuilder() { return newBuilder(this); }

        @java.lang.Override
        protected Builder newBuilderForType(
                com.google.protobuf.GeneratedMessage.BuilderParent parent) {
            Builder builder = new Builder(parent);
            return builder;
        }
        /**
         * Protobuf type {@code Breezzo.Proto.TestModelNewProto}
         */
        public static final class Builder extends
                com.google.protobuf.GeneratedMessage.Builder<Builder> implements
                // @@protoc_insertion_point(builder_implements:Breezzo.Proto.TestModelNewProto)
                com.breezzo.proto.ProtoTest.TestModelNewProtoOrBuilder {
            public static final com.google.protobuf.Descriptors.Descriptor
            getDescriptor() {
                return com.breezzo.proto.ProtoTest.internal_static_Breezzo_Proto_TestModelNewProto_descriptor;
            }

            protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
            internalGetFieldAccessorTable() {
                return com.breezzo.proto.ProtoTest.internal_static_Breezzo_Proto_TestModelNewProto_fieldAccessorTable
                        .ensureFieldAccessorsInitialized(
                                com.breezzo.proto.ProtoTest.TestModelNewProto.class, com.breezzo.proto.ProtoTest.TestModelNewProto.Builder.class);
            }

            // Construct using com.breezzo.proto.ProtoTest.TestModelNewProto.newBuilder()
            private Builder() {
                maybeForceBuilderInitialization();
            }

            private Builder(
                    com.google.protobuf.GeneratedMessage.BuilderParent parent) {
                super(parent);
                maybeForceBuilderInitialization();
            }
            private void maybeForceBuilderInitialization() {
                if (com.google.protobuf.GeneratedMessage.alwaysUseFieldBuilders) {
                }
            }
            private static Builder create() {
                return new Builder();
            }

            public Builder clear() {
                super.clear();
                testId_ = 0L;
                bitField0_ = (bitField0_ & ~0x00000001);
                testName_ = "";
                bitField0_ = (bitField0_ & ~0x00000002);
                tests_ = com.google.protobuf.LazyStringArrayList.EMPTY;
                bitField0_ = (bitField0_ & ~0x00000004);
                return this;
            }

            public Builder clone() {
                return create().mergeFrom(buildPartial());
            }

            public com.google.protobuf.Descriptors.Descriptor
            getDescriptorForType() {
                return com.breezzo.proto.ProtoTest.internal_static_Breezzo_Proto_TestModelNewProto_descriptor;
            }

            public com.breezzo.proto.ProtoTest.TestModelNewProto getDefaultInstanceForType() {
                return com.breezzo.proto.ProtoTest.TestModelNewProto.getDefaultInstance();
            }

            public com.breezzo.proto.ProtoTest.TestModelNewProto build() {
                com.breezzo.proto.ProtoTest.TestModelNewProto result = buildPartial();
                if (!result.isInitialized()) {
                    throw newUninitializedMessageException(result);
                }
                return result;
            }

            public com.breezzo.proto.ProtoTest.TestModelNewProto buildPartial() {
                com.breezzo.proto.ProtoTest.TestModelNewProto result = new com.breezzo.proto.ProtoTest.TestModelNewProto(this);
                int from_bitField0_ = bitField0_;
                int to_bitField0_ = 0;
                if (((from_bitField0_ & 0x00000001) == 0x00000001)) {
                    to_bitField0_ |= 0x00000001;
                }
                result.testId_ = testId_;
                if (((from_bitField0_ & 0x00000002) == 0x00000002)) {
                    to_bitField0_ |= 0x00000002;
                }
                result.testName_ = testName_;
                if (((bitField0_ & 0x00000004) == 0x00000004)) {
                    tests_ = tests_.getUnmodifiableView();
                    bitField0_ = (bitField0_ & ~0x00000004);
                }
                result.tests_ = tests_;
                result.bitField0_ = to_bitField0_;
                onBuilt();
                return result;
            }

            public Builder mergeFrom(com.google.protobuf.Message other) {
                if (other instanceof com.breezzo.proto.ProtoTest.TestModelNewProto) {
                    return mergeFrom((com.breezzo.proto.ProtoTest.TestModelNewProto)other);
                } else {
                    super.mergeFrom(other);
                    return this;
                }
            }

            public Builder mergeFrom(com.breezzo.proto.ProtoTest.TestModelNewProto other) {
                if (other == com.breezzo.proto.ProtoTest.TestModelNewProto.getDefaultInstance()) return this;
                if (other.hasTestId()) {
                    setTestId(other.getTestId());
                }
                if (other.hasTestName()) {
                    bitField0_ |= 0x00000002;
                    testName_ = other.testName_;
                    onChanged();
                }
                if (!other.tests_.isEmpty()) {
                    if (tests_.isEmpty()) {
                        tests_ = other.tests_;
                        bitField0_ = (bitField0_ & ~0x00000004);
                    } else {
                        ensureTestsIsMutable();
                        tests_.addAll(other.tests_);
                    }
                    onChanged();
                }
                this.mergeUnknownFields(other.getUnknownFields());
                return this;
            }

            public final boolean isInitialized() {
                return true;
            }

            public Builder mergeFrom(
                    com.google.protobuf.CodedInputStream input,
                    com.google.protobuf.ExtensionRegistryLite extensionRegistry)
                    throws java.io.IOException {
                com.breezzo.proto.ProtoTest.TestModelNewProto parsedMessage = null;
                try {
                    parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
                } catch (com.google.protobuf.InvalidProtocolBufferException e) {
                    parsedMessage = (com.breezzo.proto.ProtoTest.TestModelNewProto) e.getUnfinishedMessage();
                    throw e;
                } finally {
                    if (parsedMessage != null) {
                        mergeFrom(parsedMessage);
                    }
                }
                return this;
            }
            private int bitField0_;

            private long testId_ ;
            /**
             * <code>optional int64 test_id = 1;</code>
             */
            public boolean hasTestId() {
                return ((bitField0_ & 0x00000001) == 0x00000001);
            }
            /**
             * <code>optional int64 test_id = 1;</code>
             */
            public long getTestId() {
                return testId_;
            }
            /**
             * <code>optional int64 test_id = 1;</code>
             */
            public Builder setTestId(long value) {
                bitField0_ |= 0x00000001;
                testId_ = value;
                onChanged();
                return this;
            }
            /**
             * <code>optional int64 test_id = 1;</code>
             */
            public Builder clearTestId() {
                bitField0_ = (bitField0_ & ~0x00000001);
                testId_ = 0L;
                onChanged();
                return this;
            }

            private java.lang.Object testName_ = "";
            /**
             * <code>optional string test_name = 2;</code>
             */
            public boolean hasTestName() {
                return ((bitField0_ & 0x00000002) == 0x00000002);
            }
            /**
             * <code>optional string test_name = 2;</code>
             */
            public java.lang.String getTestName() {
                java.lang.Object ref = testName_;
                if (!(ref instanceof java.lang.String)) {
                    com.google.protobuf.ByteString bs =
                            (com.google.protobuf.ByteString) ref;
                    java.lang.String s = bs.toStringUtf8();
                    if (bs.isValidUtf8()) {
                        testName_ = s;
                    }
                    return s;
                } else {
                    return (java.lang.String) ref;
                }
            }
            /**
             * <code>optional string test_name = 2;</code>
             */
            public com.google.protobuf.ByteString
            getTestNameBytes() {
                java.lang.Object ref = testName_;
                if (ref instanceof String) {
                    com.google.protobuf.ByteString b =
                            com.google.protobuf.ByteString.copyFromUtf8(
                                    (java.lang.String) ref);
                    testName_ = b;
                    return b;
                } else {
                    return (com.google.protobuf.ByteString) ref;
                }
            }
            /**
             * <code>optional string test_name = 2;</code>
             */
            public Builder setTestName(
                    java.lang.String value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                bitField0_ |= 0x00000002;
                testName_ = value;
                onChanged();
                return this;
            }
            /**
             * <code>optional string test_name = 2;</code>
             */
            public Builder clearTestName() {
                bitField0_ = (bitField0_ & ~0x00000002);
                testName_ = getDefaultInstance().getTestName();
                onChanged();
                return this;
            }
            /**
             * <code>optional string test_name = 2;</code>
             */
            public Builder setTestNameBytes(
                    com.google.protobuf.ByteString value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                bitField0_ |= 0x00000002;
                testName_ = value;
                onChanged();
                return this;
            }

            private com.google.protobuf.LazyStringList tests_ = com.google.protobuf.LazyStringArrayList.EMPTY;
            private void ensureTestsIsMutable() {
                if (!((bitField0_ & 0x00000004) == 0x00000004)) {
                    tests_ = new com.google.protobuf.LazyStringArrayList(tests_);
                    bitField0_ |= 0x00000004;
                }
            }
            /**
             * <code>repeated string tests = 3;</code>
             */
            public com.google.protobuf.ProtocolStringList
            getTestsList() {
                return tests_.getUnmodifiableView();
            }
            /**
             * <code>repeated string tests = 3;</code>
             */
            public int getTestsCount() {
                return tests_.size();
            }
            /**
             * <code>repeated string tests = 3;</code>
             */
            public java.lang.String getTests(int index) {
                return tests_.get(index);
            }
            /**
             * <code>repeated string tests = 3;</code>
             */
            public com.google.protobuf.ByteString
            getTestsBytes(int index) {
                return tests_.getByteString(index);
            }
            /**
             * <code>repeated string tests = 3;</code>
             */
            public Builder setTests(
                    int index, java.lang.String value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                ensureTestsIsMutable();
                tests_.set(index, value);
                onChanged();
                return this;
            }
            /**
             * <code>repeated string tests = 3;</code>
             */
            public Builder addTests(
                    java.lang.String value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                ensureTestsIsMutable();
                tests_.add(value);
                onChanged();
                return this;
            }
            /**
             * <code>repeated string tests = 3;</code>
             */
            public Builder addAllTests(
                    java.lang.Iterable<java.lang.String> values) {
                ensureTestsIsMutable();
                com.google.protobuf.AbstractMessageLite.Builder.addAll(
                        values, tests_);
                onChanged();
                return this;
            }
            /**
             * <code>repeated string tests = 3;</code>
             */
            public Builder clearTests() {
                tests_ = com.google.protobuf.LazyStringArrayList.EMPTY;
                bitField0_ = (bitField0_ & ~0x00000004);
                onChanged();
                return this;
            }
            /**
             * <code>repeated string tests = 3;</code>
             */
            public Builder addTestsBytes(
                    com.google.protobuf.ByteString value) {
                if (value == null) {
                    throw new NullPointerException();
                }
                ensureTestsIsMutable();
                tests_.add(value);
                onChanged();
                return this;
            }

            // @@protoc_insertion_point(builder_scope:Breezzo.Proto.TestModelNewProto)
        }

        static {
            defaultInstance = new TestModelNewProto(true);
            defaultInstance.initFields();
        }

        // @@protoc_insertion_point(class_scope:Breezzo.Proto.TestModelNewProto)
    }

    private static final com.google.protobuf.Descriptors.Descriptor
            internal_static_Breezzo_Proto_TestModelNewProto_descriptor;
    private static
    com.google.protobuf.GeneratedMessage.FieldAccessorTable
            internal_static_Breezzo_Proto_TestModelNewProto_fieldAccessorTable;

    public static com.google.protobuf.Descriptors.FileDescriptor
    getDescriptor() {
        return descriptor;
    }
    private static com.google.protobuf.Descriptors.FileDescriptor
            descriptor;
    static {
        java.lang.String[] descriptorData = {
                "\n\017ProtoTest.proto\022\rBreezzo.Proto\"F\n\021Test" +
                        "ModelNewProto\022\017\n\007test_id\030\001 \001(\003\022\021\n\ttest_n" +
                        "ame\030\002 \001(\t\022\r\n\005tests\030\003 \003(\tB\'\n\021com.breezzo." +
                        "protoB\tProtoTest\200\001\000\210\001\000\220\001\000"
        };
        com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner assigner =
                new com.google.protobuf.Descriptors.FileDescriptor.    InternalDescriptorAssigner() {
                    public com.google.protobuf.ExtensionRegistry assignDescriptors(
                            com.google.protobuf.Descriptors.FileDescriptor root) {
                        descriptor = root;
                        return null;
                    }
                };
        com.google.protobuf.Descriptors.FileDescriptor
                .internalBuildGeneratedFileFrom(descriptorData,
                        new com.google.protobuf.Descriptors.FileDescriptor[] {
                        }, assigner);
        internal_static_Breezzo_Proto_TestModelNewProto_descriptor =
                getDescriptor().getMessageTypes().get(0);
        internal_static_Breezzo_Proto_TestModelNewProto_fieldAccessorTable = new
                com.google.protobuf.GeneratedMessage.FieldAccessorTable(
                internal_static_Breezzo_Proto_TestModelNewProto_descriptor,
                new java.lang.String[] { "TestId", "TestName", "Tests", });
    }

    // @@protoc_insertion_point(outer_class_scope)
}