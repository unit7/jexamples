package com.breezzo.proto;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by breezzo on 28.12.16.
 */
public class TestWrite {
    public static void main(String[] args) throws IOException {
        ProtoTest.TestModelNewProto proto = ProtoTest.TestModelNewProto.newBuilder()
                .setTestId(1L)
                .setTestName("qwerty")
                .addTests("qwerty1")
                .addTests("qwerty2")
                .addTests("qwerty3")
                .build();

        try (OutputStream out = new FileOutputStream("/home/breezzo/proto_test.pb")) {
            proto.writeTo(out);
        }
    }
}
