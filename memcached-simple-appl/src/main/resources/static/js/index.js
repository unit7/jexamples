(function($) {

    function loadCypheredData(data) {
        if (!data || !data.length) {
            $('#resources').text('no data');
            return;
        }

        var list = $('#resources').append('<ul></ul>');

        for (var i = 0; i < data.length; ++i) {
            var dataItem = data[i];
            list.append('<li><a href="/rest/cypher/data/' + dataItem.id + '">' + dataItem.shortName + '</a></li>');
        }
    }

    function loadErrorInfo(xhr) {
        $('#resources').append('<span class="error">' + xhr.responseText + '</span>');
    }

    function initialize() {
        $.ajax({
            url: '/rest/cypher/data',
            type: 'GET'
        })
            .done(loadCypheredData)
            .fail(loadErrorInfo);
    }

    initialize();

})(jQuery);