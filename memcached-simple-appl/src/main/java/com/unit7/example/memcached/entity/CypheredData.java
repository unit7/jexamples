package com.unit7.example.memcached.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Arrays;

/**
 * Created by breezzo on 20.03.16.
 */
@Entity
@Table(name = "cyphered_data")
public class CypheredData implements Serializable {

    @Id
    @GenericGenerator(name = "id_simple_gen", strategy = "increment")
    @GeneratedValue(generator = "id_simple_gen")
    @Column(name = "id")
    private Long id;

    @Column(name = "short_name")
    private String shortName;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "data")
    private byte[] data;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CypheredData{");
        sb.append("id=").append(id);
        sb.append(", shortName='").append(shortName).append('\'');
        sb.append(", data=").append(data);
        sb.append('}');
        return sb.toString();
    }
}
