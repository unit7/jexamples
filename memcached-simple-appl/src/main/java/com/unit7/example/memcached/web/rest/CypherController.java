package com.unit7.example.memcached.web.rest;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.unit7.example.memcached.entity.CypheredData;
import com.unit7.example.memcached.exception.RestException;
import com.unit7.example.memcached.repository.CypheredDataRepository;
import com.unit7.example.memcached.web.rest.json.CypheredDataJson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by breezzo on 19.03.16.
 */
@RestController
@RequestMapping("rest/cypher")
public class CypherController {

    private static final Logger logger = LoggerFactory.getLogger(CypherController.class);

    @Autowired
    private CypheredDataRepository cypheredDataRepository;

    @RequestMapping(value = "data", method = RequestMethod.GET)
    public List<CypheredDataJson> all() {
        Iterable<CypheredData> allData = cypheredDataRepository.findAll();
        logger.debug("retrieved data: {}", allData);

        return Lists.newArrayList(Iterables.transform(allData, CypheredDataJson::new));
    }

    @RequestMapping(value = "data", method = RequestMethod.POST)
    public Long createOne(@RequestParam("file") MultipartFile file) {

        logger.debug("try to create instance with name {}, size {} bytes", file.getOriginalFilename(), file.getSize());

        CypheredData cypheredData = new CypheredData();

        try {
            cypheredData.setShortName(file.getOriginalFilename());
            cypheredData.setData(file.getBytes());

            cypheredData = cypheredDataRepository.save(cypheredData);
        } catch (Exception e) {
            logger.error("error occurred while saving entity: {}", e.getMessage());
            throw new RestException(e.getMessage(), e);
        }

        logger.debug("instance created: {}", cypheredData);

        return cypheredData.getId();
    }

    @RequestMapping(value = "data/{id}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_OCTET_STREAM_VALUE })
    public void downloadData(@PathVariable("id") Long dataId, HttpServletResponse response) {
        logger.debug("try to find data with id: {}", dataId);

        CypheredData one = cypheredDataRepository.findOne(dataId);

        logger.info("found data: {}", one);

        try {
            response.getOutputStream().write(one.getData());
        } catch (Exception e) {
            logger.error("Error occurred while downloading data: {}", e.getMessage());
            throw new RestException(e.getMessage(), e);
        }

        response.addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_OCTET_STREAM_VALUE);
    }
}
