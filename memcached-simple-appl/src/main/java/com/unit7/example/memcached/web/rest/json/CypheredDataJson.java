package com.unit7.example.memcached.web.rest.json;

import com.unit7.example.memcached.entity.CypheredData;

/**
 * Created by breezzo on 20.03.16.
 */
public class CypheredDataJson {
    private CypheredData cypheredData;

    public CypheredDataJson(CypheredData cypheredData) {
        this.cypheredData = cypheredData;
    }

    public Long getId() {
        return cypheredData.getId();
    }

    public void setId(Long id) {
        cypheredData.setId(id);
    }

    public String getShortName() {
        return cypheredData.getShortName();
    }

    public void setShortName(String shortName) {
        cypheredData.setShortName(shortName);
    }
}
