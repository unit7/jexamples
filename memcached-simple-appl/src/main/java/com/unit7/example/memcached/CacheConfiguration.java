package com.unit7.example.memcached;

import com.google.code.ssm.CacheFactory;
import com.google.code.ssm.config.AddressProvider;
import com.google.code.ssm.config.DefaultAddressProvider;
import com.google.code.ssm.providers.CacheClientFactory;
import com.google.code.ssm.providers.xmemcached.MemcacheClientFactoryImpl;
import com.google.code.ssm.spring.SSMCache;
import com.google.code.ssm.spring.SSMCacheManager;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.util.Collections;

/**
 * Created by breezzo on 21.03.16.
 */
@Configuration
public class CacheConfiguration {

    @Bean
    public CacheFactory cacheFactory() {
        CacheFactory factory = new CacheFactory();

        factory.setCacheName("defaultCache");
        factory.setCacheClientFactory(cacheClientFactory());
        factory.setAddressProvider(addressProvider());
        factory.setConfiguration(cacheConfigurationBean());

        return factory;
    }

    @Bean
    public CacheClientFactory cacheClientFactory() {
        return new MemcacheClientFactoryImpl();
    }

    @Bean
    public AddressProvider addressProvider() {
        return new DefaultAddressProvider("127.0.0.1:11211");
    }

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public com.google.code.ssm.providers.CacheConfiguration cacheConfigurationBean() {
        com.google.code.ssm.providers.CacheConfiguration configuration = new com.google.code.ssm.providers.CacheConfiguration();
        configuration.setConsistentHashing(true);

        return configuration;
    }

    @Bean
    public SSMCacheManager cacheManager() throws Exception {
        SSMCacheManager manager = new SSMCacheManager();
        SSMCache ssmCache = new SSMCache(cacheFactory().getObject(), 300, false);

        manager.setCaches(Collections.singletonList(ssmCache));

        return manager;
    }

}
