package com.unit7.example.memcached.repository;

import com.unit7.example.memcached.entity.CypheredData;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by breezzo on 20.03.16.
 */
public interface CypheredDataRepository extends CrudRepository<CypheredData, Long> {

    @Cacheable("defaultCache")
    @Override
    CypheredData findOne(Long id);
}