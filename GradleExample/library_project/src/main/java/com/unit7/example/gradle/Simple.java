package com.unit7.example.gradle;

public class Simple {
	private int value;

	public void setValue(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}
}