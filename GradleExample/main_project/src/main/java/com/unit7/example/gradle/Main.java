package com.unit7.example.gradle;

import org.apache.log4j.Logger;

public class Main {
	private static final Logger logger = Logger.getLogger(Main.class);

	public static void main(String[] args) {
		Simple simple = new Simple();
		simple.setValue(12);

		logger.info("Application started");
		System.out.println("Application sample output. Value: " + simple.getValue());
		logger.info("Application finished");
	}
}